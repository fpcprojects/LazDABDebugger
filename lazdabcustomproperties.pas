unit LazDABCustomProperties;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  DbgIntfDebuggerBase;

type
  { TLazDABCustomProperties }

  TLazDABCustomProperties = class(TDebuggerProperties)
  private
    FShowDABCommunicationInEventLog: Boolean;
  public
    procedure Assign(Source: TPersistent); override;
  published
    // Setting this to True makes the debugger really, really slow, because the
    // EventLog is truely slow when there are more then, say 10, events in the
    // log.
    property ShowDABCommunicationInEventLog: Boolean read FShowDABCommunicationInEventLog write FShowDABCommunicationInEventLog default False;
  end;


implementation

{ TLazDABCustomProperties }

procedure TLazDABCustomProperties.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TLazDABCustomProperties then
    FShowDABCommunicationInEventLog := TLazDABCustomProperties(Source).ShowDABCommunicationInEventLog;
end;

end.

