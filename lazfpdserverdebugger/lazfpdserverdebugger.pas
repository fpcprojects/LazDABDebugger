{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit LazFPDServerDebugger;

{$warn 5023 off : no warning about unused units}
interface

uses
  LazFPDServerRegistration, LazFPDServerProperties, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('LazFPDServerRegistration', @LazFPDServerRegistration.Register);
end;

initialization
  RegisterPackage('LazFPDServerDebugger', @Register);
end.
