unit LazFPDServerProperties;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  DbgIntfDebuggerBase,
  LazDABCustomProperties;

type

  { TLazFPDServerProperties }

  TLazFPDServerProperties = class(TLazDABCustomProperties)
  private
    FDABAdapterHostname: string;
    FDABAdapterPort: integer;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property DABAdapterHostname: string read FDABAdapterHostname write FDABAdapterHostname;
    property DABAdapterPort: integer read FDABAdapterPort write FDABAdapterPort;
  end;


implementation

{ TLazFPDServerProperties }

procedure TLazFPDServerProperties.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TLazFPDServerProperties then
    begin
    FDABAdapterHostname := TLazFPDServerProperties(Source).DABAdapterHostname;
    FDABAdapterPort := TLazFPDServerProperties(Source).DABAdapterPort;
    end;
end;

end.

