{ Implementation of the FPDServer debugging interfaces to communicate with the
  Lazarus IDE

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazFPDServerRegistration;

interface

uses
  Classes,
  SysUtils,
  fpjson,
  Generics.Collections,
  process,
  ssockets,
  UTF8Process,
  DbgIntfDebuggerBase,
  LazDebugExtensionDebuggerIntf,
  LazDABCustomDebugger,
  LazDabGlue,
  LazDABCommunication,
  LazFPDServerProperties,
  Forms;

type

  { TLazFPDServerDebugger }

  TLazFPDServerDebugger = class(TLazCustomDABDebugger)
  protected
    function GetDABConnectionType: TLazDABConnectionType; override;
    function GetMustCreateDABProcess: Boolean; override;
    procedure GetTcpIpProperties(out AHost: string; out APort: Integer); override;
  public
    class function CreateProperties: TDebuggerProperties; override;
    class function Caption: String; override;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterDebugger(TLazFPDServerDebugger);
end;

{ TLazFPDServerDebugger }

class function TLazFPDServerDebugger.Caption: string;
begin
  Result := 'FPDServer Debugger';
end;

function TLazFPDServerDebugger.GetDABConnectionType: TLazDABConnectionType;
var
  AHost: string;
begin
  AHost := TLazFPDServerProperties(GetProperties).DABAdapterHostname;
  if AHost<>'' then
    Result := dctTcpIp
  else
    Result := dctSocket;
end;

procedure TLazFPDServerDebugger.GetTcpIpProperties(out AHost: string; out APort: Integer);
begin
  AHost := TLazFPDServerProperties(GetProperties).DABAdapterHostname;
  APort := TLazFPDServerProperties(GetProperties).DABAdapterPort;
end;

class function TLazFPDServerDebugger.CreateProperties: TDebuggerProperties;
begin
  Result := TLazFPDServerProperties.Create;
end;

function TLazFPDServerDebugger.GetMustCreateDABProcess: Boolean;
var
  AHost: string;
begin
  AHost := TLazFPDServerProperties(GetProperties).DABAdapterHostname;
  Result := (AHost = '');
end;

end.

