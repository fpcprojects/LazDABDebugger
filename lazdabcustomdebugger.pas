{ Implementation of the debugging interfaces to communicate with the Lazarus IDE

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDABCustomDebugger;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  fpjson,
  Generics.Collections,
  process,
  ssockets,
  UTF8Process,
  DbgIntfDebuggerBase,
  DABMessages,
  LazDebuggerIntf,
  LazDebuggerIntfBaseTypes,
  LazDebugExtensionDebuggerIntf,
  LazDabGlue,
  LazDABCommunication,
  LazDABCustomProperties,
  Forms;

type
  // A continue-command to the DAB-Adapter has the following modes:
  TLazDabContinueType = (ctContinue, ctNext, ctStepIn, ctStepOut);
  // How to connect with the DAB-Adapter
  TLazDABConnectionType = (dctTcpIp, dctSocket);
  TLazDABThreadsCallback = procedure(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AThreadList:TDABThreadList) of object;

  { TLazDABCommand }

  TLazCustomDABDebugger = class;
  TLazDABCommand = class
  protected
    FSeq: Integer;
    FCommunicationThread: TLazDABCommunicationThread;
    FDebugger: TLazCustomDABDebugger;
    procedure SetState(const AValue: TDBGState);
    procedure SetErrorState(const AMsg: String; const AInfo: String);
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger);
    procedure Execute(out AnIsFinished: Boolean); virtual; abstract;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); virtual;
    property Seq: Integer read FSeq;
  end;
  TLazDABCommandDictionary = specialize TObjectDictionary<Integer, TLazDABCommand>;

  { TLazCustomDABDebugger }

  TLazCustomDABDebugger = class(TDebuggerIntf, ILazDebugExtensionDebuggerIntf)
  private
    FInitializationRunning: Boolean;
    FCurrentStackFrame: Integer;
    // Either FDABProcess or FTcpIpSocket is used
    FDABProcess: TProcessUTF8;
    FTcpIpSocket: TInetSocket;
    FCurrentThreadId: Integer;
    procedure EmergencyStop(const Message: string);
  protected
    FCommunicationThread: TLazDABCommunicationThread;
    FCommandDictionary: TLazDABCommandDictionary;

    procedure ReceiveResponse(const Response: TDABResponse);
    procedure ReceiveEvent(const Event: TDABEvent);
    procedure OnLog(const Message: string);
    procedure OnLogCommunication(const Message: string);
    procedure OnLogError(const Message: string);

    procedure HandleOutputEvent(const Event: TDABOutputEvent);
    procedure HandleStoppedEvent(const Event: TDABStoppedEvent);

    procedure RunCommand(ACommand: TLazDABCommand);
    function CreateAttachOrLaunchCommand(const AParams: array of const; const ACallback: TMethod): TLazDABCommand; virtual;

    function DoRun(const AParams: array of const; const ACallback: TMethod): Boolean;
    procedure DoContinue(AContinueType: TLazDabContinueType);
    procedure DoPause;
    procedure DoStop;
    function DoEvaluate(const AnExpression: String; EvalFlags: TWatcheEvaluateFlags; ACallback: TDBGEvaluateResultCallback): Boolean;
    function CreateCallStack: TCallStackSupplier; override;
    function CreateBreakPoints: TDBGBreakPoints; override;
    function CreateThreads: TThreadsSupplier; override;
    function RequestCommand(const ACommand: TDBGCommand;
                            const AParams: array of const;
                            const ACallback: TMethod): Boolean; override;
    procedure DoState(const OldState: TDBGState); override;

    function CreateDABProcess: Boolean; virtual;
  protected
    // These functions should be overridden by descendants to set the proper
    // DAB-initialization behaviour for a specific DAB-Adapter.
    function GetDABConnectionType: TLazDABConnectionType; virtual;
    function GetMustCreateDABProcess: Boolean; virtual;
    procedure GetTcpIpProperties(out AHost: string; out APort: Integer); virtual;
    procedure SetAdditionalDABProcessOptions(AProcess: TProcessUTF8); virtual;
  public
    constructor Create(const AExternalDebugger: String); override;
    destructor Destroy; override;

    procedure Init; override;
    procedure Done; override;

    procedure RequestCallstack(AThreadId, ACount: Integer; AStackList: TCallStackBase; ADoCurrent: Boolean);
    procedure SetCurrentLocation(ALocation: TDBGLocationRec; AStackFrameId: Integer);

    function RequestThreads(const ACallBack: TLazDABThreadsCallback): Integer;

    class function NeedsExePath: boolean; override;
    class function GetSupportedCommands: TDBGCommands; override;
    class function CreateProperties: TDebuggerProperties; override;

    // ILazDebugExtensionDebuggerIntf
    function EvaluateExpression(const AnExpression: string; const AFrameId: Integer; const ARef: PtrInt; const ACallBack: TLazDebugExtensionEvaluateCallback): Boolean;
    function RequestVariables(const AFrameId: Integer; const ARef: PtrInt; const ACallBack: TLazDebugExtensionVariablesCallback): Boolean;
    function LoadChilds(const AVariable: TLazDebugExtensionVariable; const ARef: PtrInt; const ACallBack: TLazDebugExtensionLoadChildsCallback): Boolean;
    function GetCurrentFrameID: Integer;
    function HasChildren(const AVariable: TLazDebugExtensionVariable): Boolean;

    // (Re-)sets all breakpoints. Is public so it can be triggered when an
    // individual breakpoint has been changed.
    // Returns True if a command has been send to the DAB-Adapter. (Used during
    // the initialization-fase)
    function SetBreakpoints: Boolean;

    function GetEvaluationState: TLazExtEvaluationState;
  end;

implementation

uses
  LazDABCommand;

{ TLazDABCommand }

procedure TLazDABCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
begin
  AnIsFinished := True;
end;

constructor TLazDABCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger);
begin
  FDebugger := ADebugger;
  FCommunicationThread := ACommunicationThread;
end;

procedure TLazDABCommand.SetState(const AValue: TDBGState);
begin
  FDebugger.SetState(AValue);
end;

procedure TLazDABCommand.SetErrorState(const AMsg: string; const AInfo: string);
begin
  FDebugger.SetErrorState(AMsg, AInfo);
end;

{ TLazCustomDABDebugger }

constructor TLazCustomDABDebugger.Create(const AExternalDebugger: string);
begin
  inherited Create(AExternalDebugger);
  FCommandDictionary := TLazDABCommandDictionary.Create;
end;

class function TLazCustomDABDebugger.NeedsExePath: boolean;
begin
  Result := True;
end;

class function TLazCustomDABDebugger.GetSupportedCommands: TDBGCommands;
begin
  Result := [dcRun, dcStepOver, dcStepInto, dcStepOut, dcPause, dcEvaluate, dcStop];
end;

function TLazCustomDABDebugger.RequestCommand(const ACommand: TDBGCommand; const AParams: array of const; const ACallback: TMethod): Boolean;
var
  EvalFlags: TWatcheEvaluateFlags;
begin
  Result := True;
  case ACommand of
    dcRun:      Result := DoRun(AParams, ACallback);
    dcStepOver: DoContinue(ctNext);
    dcStepInto: DoContinue(ctStepIn);
    dcStepOut:  DoContinue(ctStepOut);
    dcPause:    DoPause;
    dcStop:     DoStop;
    dcEvaluate:
      begin
      EvalFlags := [];
      if high(AParams) >= 1 then
        EvalFlags := TWatcheEvaluateFlags(AParams[1].VInteger);
      Result := DoEvaluate(String(AParams[0].VAnsiString),
        EvalFlags, TDBGEvaluateResultCallback(ACallback));
      end;
  else
    raise Exception.Create('Unknown command: ');
  end;
end;

type
  TPairKeyValue = specialize TPair<Integer, TLazDABCommand>;

destructor TLazCustomDABDebugger.Destroy;
var
  Pair: TPairKeyValue;
begin
  TLazDebugExtensionDebugManager.MarkDebuggerDisposed(Self);

  // Also see the code in Done, where the debugging is actually stopped.
  inherited Destroy;

  // It might be that some commands are still there (for example: a
  // TLazDABDisconnectCommand)
  for Pair in FCommandDictionary do
    Pair.Value.Free;
  FCommandDictionary.Free;
end;

function TLazCustomDABDebugger.DoRun(const AParams: array of const; const ACallback: TMethod): Boolean;
var
  RCommand: TLazDABCommand;
begin
  if State in [dsPause, dsInternalPause] then
    RCommand := TLazDABContinueCommand.Create(FCommunicationThread, Self, ctContinue)
  else
    begin
    RCommand := CreateAttachOrLaunchCommand(AParams, ACallback);
    end;
  RunCommand(RCommand);
end;

procedure TLazCustomDABDebugger.Init;
var
  InitializeCommand: TLazDABInitializeCommand;
  AHost: string;
  APort: Integer;
  InStream: THandleStream;
  OutStream: THandleStream;
begin
  if Assigned(FCommunicationThread) then
    raise Exception.Create('Communication thread already running');

  if GetMustCreateDABProcess then
    if not CreateDABProcess then
      Exit; // Error-state is already set by CreateDABProcess;

  case GetDABConnectionType of
    dctSocket:
      begin
      InStream := FDABProcess.Output;
      OutStream := FDABProcess.Input;
      end;
    dctTcpIp:
      begin
      GetTcpIpProperties(AHost, APort);
      try
        FTcpIpSocket := TInetSocket.Create(AHost, APort);
      except
        on E: Exception do
          begin
          SetErrorState('Failed to connect with DAB-Adapter.', E.Message);
          Exit;
          end;
      end;
      InStream := FTcpIpSocket;
      OutStream := FTcpIpSocket;
      end;
  end;
  try
    FCommunicationThread := TLazDABCommunicationThread.Create(InStream, OutStream, @ReceiveResponse, @ReceiveEvent, @OnLog, @OnLogCommunication, @OnLogError, @EmergencyStop);
  except
    on E: Exception do
      begin
      SetErrorState('Failed to start communication with DAB-Adapter.', E.Message);
      Exit;
      end;
  end;

  InitializeCommand := TLazDABInitializeCommand.Create(FCommunicationThread, Self);
  RunCommand(InitializeCommand);

  // It is not possible to call the inherited Init earlier, because it sets the
  // state to dsIdle.
  // We only want to become idle when there occurred no exceptions in the code
  // above. (for example: no connection with the DAB-server)
  inherited Init;

  TLazDebugExtensionDebugManager.MarkActiveDebugger(Self);
end;

procedure TLazCustomDABDebugger.ReceiveResponse(const Response: TDABResponse);
var
  Command: TLazDABCommand;
  IsFinished: Boolean;
  AllBreakpointsDone: Boolean;
  Pair: TPairKeyValue;
begin
  if FCommandDictionary.TryGetValue(Response.Request_seq, Command) then
    begin
    Command.HandleResponse(Response, IsFinished);
    if IsFinished then
      begin
      FCommandDictionary.Remove(Response.Request_seq);
      Command.Free;
      end;
    end;
  Response.Free;
  if FInitializationRunning then
    begin
    // When all TLazDABSetBreakpointsCommand are done, the initialization is done
    // and we can send ConfigurationDone to the DAB-Adapter.

    // It is not possible to do a simple check to see if all queued commands are
    // done, as at least one DAP-Adapter (DebugPy) only sends a response to the
    // Attach-request after it receives a ConfigurationDone. (So at this point
    // there will always be an Attach-command within the CommandDictionary when
    // connected with DebugPy)
    AllBreakpointsDone := True;
    for Pair in FCommandDictionary do
      if Pair.Value is TLazDABSetBreakpointsCommand then
        AllBreakpointsDone := False;
    if AllBreakpointsDone then
      begin
      FInitializationRunning := False;
      RunCommand(TLazDABConfigurationDoneCommand.Create(FCommunicationThread, Self));
      end;
    end;
end;

procedure TLazCustomDABDebugger.RunCommand(ACommand: TLazDABCommand);
var
  IsFinished: Boolean;
begin
  ACommand.Execute(IsFinished);
  if not IsFinished then
    FCommandDictionary.Add(ACommand.Seq, ACommand)
  else
    ACommand.Free;
end;

procedure TLazCustomDABDebugger.ReceiveEvent(const Event: TDABEvent);
begin
  if SameText(Event.Event, 'initialized') then
    begin
    if SetBreakPoints then
      begin
      // If i read the specifications correctly, there in no need to wait before
      // sending the ConfigurationDone-command after the TLazDABSetBreakpointsCommands
      // are sent.
      // But it 'feels better' to wait until all the breakpoint-commands are
      // executed (until a response has been received). (VSCode does the same.)
      FInitializationRunning := True;
      end
    else
      // There are no breakpoint that have to be set (and for now there are no
      // other tasks) so the configuration is done.
      RunCommand(TLazDABConfigurationDoneCommand.Create(FCommunicationThread, Self));
    end
  else if SameText(Event.Event, 'exited') then
    begin
    //SetExitCode((Event.Body as TDABExitedEventBody).ExitCode);
    SetState(dsStop);
    end
  else if SameText(Event.Event, 'terminated') then
    begin
    // Note that in some cases (IDE shutdown) the next line calls ResetDebugger,
    // after which the debugger (ie: this object itself) has been freed. So
    // you can not not access 'self' or anything similar after this line.
    SetState(dsStop);
    end
  else if SameText(Event.Event, 'output') then
    begin
    HandleOutputEvent(Event as TDABOutputEvent);
    end
  else if SameText(Event.Event, 'stopped') then
    begin
    HandleStoppedEvent(Event as TDABStoppedEvent);
    end;

  Event.Free;
end;

function TLazCustomDABDebugger.SetBreakpoints: Boolean;

var
  Res: Boolean;

  procedure SendCmd(Source: string; Lines: TLazDABBreakpointArray);
  var
    BPCommand: TLazDABSetBreakpointsCommand;
  begin
    if Length(Lines)> 0 then
      begin
      BPCommand := TLazDABSetBreakpointsCommand.Create(FCommunicationThread, Self, Source, Lines);
      RunCommand(BPCommand);
      Res := True;
      end;
  end;

var
  i: Integer;
  Breakpoint: TDBGBreakPoint;
  SortedBPList: TStringList;
  CurrentSource: string;
  Lines: TLazDABBreakpointArray;
begin
  Res := False;
  SortedBPList := TStringList.Create;
  try
    SortedBPList.Duplicates := dupAccept;
    SortedBPList.Sorted := true;
    SortedBPList.CaseSensitive := False;
    for i := 0 to BreakPoints.Count -1 do
      begin
      if BreakPoints[i].Enabled then
        SortedBPList.AddObject(BreakPoints[i].Source, BreakPoints[i]);
      end;

    CurrentSource := '';
    SetLength(Lines, 0);
    for i := 0 to SortedBPList.Count -1 do
      begin
      BreakPoint := SortedBPList.Objects[i] as TDBGBreakPoint;
      if CurrentSource<>Breakpoint.Source then
        begin
        SendCmd(CurrentSource, Lines);
        SetLength(Lines, 0);
        CurrentSource := Breakpoint.Source;
        end;
      Lines := Concat(Lines, [Breakpoint]);
      end;
    SendCmd(CurrentSource, Lines);
  finally
    SortedBPList.Free;
  end;
  Result := Res;
end;

procedure TLazCustomDABDebugger.OnLog(const Message: string);
begin
  if Assigned(EventLogHandler) then
    EventLogHandler.LogCustomEvent(ecOutput, etOutputDebugString, Message)
end;

procedure TLazCustomDABDebugger.OnLogError(const Message: string);
begin
  EventLogHandler.LogCustomEvent(ecDebugger, etOutputDebugString, Message);
end;

function TLazCustomDABDebugger.CreateBreakPoints: TDBGBreakPoints;
begin
  Result := TDBGBreakPoints.Create(Self, TLazDabBreakpoint);
end;

procedure TLazCustomDABDebugger.HandleOutputEvent(const Event: TDABOutputEvent);
begin
  case TDABOutputEventBody(Event.Body).Category of
    'stdout',
    'stderr': OnConsoleOutput(self, TDABOutputEventBody(Event.Body).Output);
    'important': OnFeedback(self, 'Received an important message from the debugger: ' + TDABOutputEventBody(Event.Body).Output, '', ftWarning, [frOk]);
  else if Assigned(EventLogHandler) then
    EventLogHandler.LogCustomEvent(ecOutput, etOutputDebugString, 'Received DAB (' + TDABOutputEventBody(Event.Body).Category + ') output-event : ' + TDABOutputEventBody(Event.Body).Output)
  end;
end;

procedure TLazCustomDABDebugger.HandleStoppedEvent(const Event: TDABStoppedEvent);
var
  BrkId, i, j: Integer;
  Brk: TDBGBreakPoint;
  Continue: boolean;
  Command: TLazDABCommand;
  LocRec: TDBGLocationRec;
begin
  Continue := False;
  for i := 0 to High(Event.Body.HitBreakpointIds) do
    begin
    BrkId := Event.Body.HitBreakpointIds[i];
    Brk := nil;
    for j := 0 to BreakPoints.Count -1 do
      begin
      if (BreakPoints[j] as TLazDabBreakpoint).DABId=BrkId then
        begin
        Brk := BreakPoints[j];
        Break;
        end;
      end;
    if Assigned(brk) then
      Brk.Hit(Continue)
    else
      // We did hit a breakpoint which is no longer available. This means it has
      // been removed, so we have to continue.
      Continue := True;
    end;

  if Event.Body.Reason='exception' then
    begin
    LocRec.Address := 0;
    LocRec.FuncName := '';
    LocRec.SrcFile := '';
    LocRec.SrcFullName := '';
    LocRec.SrcLine := 0;
    DoException(deExternal, Event.Body.Text, LocRec, '', Continue);
    end;

  if Continue then
    begin
    Command := TLazDABContinueCommand.Create(FCommunicationThread, Self, ctContinue);
    RunCommand(Command);
    Exit;
    end;

  TLazDABThreadsSupplier(Threads).ChangeCurrentThread(Event.Body.ThreadId);
  SetState(dsPause);
  // ToDo: This leads to two calls to retrieve the callstack when the callstack-
  // window is open, which requests the callstack on the state-change to dsPause.
  // But it has no effect to do this call when the state is still dsRun.
  (CallStack as TLazDABCallStackSupplier).RequestCurrentLocation(Event.Body.ThreadId);
end;

function TLazCustomDABDebugger.CreateCallStack: TCallStackSupplier;
begin
  Result := TLazDABCallStackSupplier.Create(Self);
end;

procedure TLazCustomDABDebugger.RequestCallstack(AThreadId, ACount: Integer; AStackList: TCallStackBase; ADoCurrent: Boolean);
var
  Command: TLazDABStackTraceCommand;
begin
  Command := TLazDABStackTraceCommand.Create(FCommunicationThread, Self, AThreadId, ACount, AStackList, ADoCurrent);
  AStackList.SetCountValidity(ddsRequested);
  RunCommand(Command);
end;

function TLazCustomDABDebugger.DoEvaluate(const AnExpression: string; EvalFlags: TWatcheEvaluateFlags; ACallback: TDBGEvaluateResultCallback): Boolean;
var
  Command: TLazDABEvaluateCommand;
begin
  Command := TLazDABEvaluateCommand.Create(FCommunicationThread, Self, AnExpression, FCurrentStackFrame, ACallback);
  RunCommand(Command);
  Result := True;
end;

procedure TLazCustomDABDebugger.SetCurrentLocation(ALocation: TDBGLocationRec; AStackFrameId: Integer);
begin
  FCurrentStackFrame := AStackFrameId;
  DoCurrent(ALocation);
  // Only now, once the current stack frame is known, it is possible to evaluate
  // variables.
  TLazDebugExtensionDebugManager.SendEvent(edeEvalutionStateChanged);
end;

function TLazCustomDABDebugger.EvaluateExpression(const AnExpression: string; const AFrameId: Integer; const ARef: PtrInt; const ACallBack: TLazDebugExtensionEvaluateCallback): Boolean;
var
  Command: TLazDABExtEvaluateCommand;
begin
  Command := TLazDABExtEvaluateCommand.Create(FCommunicationThread, Self, AnExpression, FCurrentStackFrame, ARef, ACallback);
  RunCommand(Command);
  Result := True;
end;

function TLazCustomDABDebugger.GetCurrentFrameID: Integer;
begin
  Result := FCurrentStackFrame;
end;

function TLazCustomDABDebugger.HasChildren(const AVariable: TLazDebugExtensionVariable): Boolean;
begin
  Result := Assigned(AVariable) and (AVariable.Reference>0);
end;

function TLazCustomDABDebugger.LoadChilds(const AVariable: TLazDebugExtensionVariable; const ARef: PtrInt; const ACallBack: TLazDebugExtensionLoadChildsCallback): Boolean;
var
  Command: TLazDABExtVariablesCommand;
begin
  Command := TLazDABExtVariablesCommand.Create(FCommunicationThread, Self, AVariable.Reference, 0, 0, ARef, ACallback);
  RunCommand(Command);
  Result := True;
end;

function TLazCustomDABDebugger.GetEvaluationState: TLazExtEvaluationState;
begin
  case State of
    dsRun:
      Result := esEvaluationPaused;
    dsPause, dsInternalPause:
      if FCurrentStackFrame>-1 then
        Result := esEvaluationPossible
      else
        Result := esEvaluationPaused
  else
    Result := esEvaluationImpossible;
  end;
end;

procedure TLazCustomDABDebugger.DoState(const OldState: TDBGState);
begin
  if (OldState in [dsPause, dsInternalPause, dsRun]) and (not (State in [dsPause, dsInternalPause, dsRun])) then
    begin
    // Change into esEvaluationImpossible
    TLazDebugExtensionDebugManager.SendEvent(edeEvalutionStateChanged);
    FCurrentStackFrame := -1;
    end
  else if (State in [dsRun]) then
    begin
    // Change into esEvaluationPaused;
    TLazDebugExtensionDebugManager.SendEvent(edeEvalutionStateChanged);
    FCurrentStackFrame := -1;
    end;
  // In some cases (IDE shutdown) the next line calls ResetDebugger, after which
  // the debugger (ie: this object itself) has been freed. So you can not not
  // access 'self' or anything similar after this line.
  inherited DoState(OldState);
end;

procedure TLazCustomDABDebugger.DoContinue(AContinueType: TLazDabContinueType);
var
  RCommand: TLazDABContinueCommand;
begin
  RCommand := TLazDABContinueCommand.Create(FCommunicationThread, Self, AContinueType);
  RunCommand(RCommand);
end;

procedure TLazCustomDABDebugger.DoPause;
var
  RCommand: TLazDABPauseCommand;
begin
  RCommand := TLazDABPauseCommand.Create(FCommunicationThread, Self);
  RunCommand(RCommand);
end;

procedure TLazCustomDABDebugger.DoStop;
var
  DCommand: TLazDABDisconnectCommand;
begin
  DCommand := TLazDABDisconnectCommand.Create(FCommunicationThread, Self);
  RunCommand(DCommand);
end;

procedure TLazCustomDABDebugger.Done;
begin
  // Done could be called multiple times...
  if Assigned(FCommunicationThread) then
    begin
    DoStop; // The command should come through, but we probably miss the response.
    FCommunicationThread.Terminate;
    FCommunicationThread.CloseConnection;
    FCommunicationThread.WaitFor;
    // The communication-thread could have sent messages on the AsyncCallQueue.
    // These have to be processed before this thread, or this debugger even, has
    // been destroyed. (Which probably will happen very soon at this moment...)
    Application.ProcessMessages;
    FreeAndNil(FCommunicationThread);
    FreeAndNil(FTcpIpSocket);

    if (FDABProcess <> nil) then
      begin
      if (FDABProcess.Running) then
        FDABProcess.Terminate(0);
      try
        FreeAndNil(FDABProcess);
      except
        on E: Exception do
          SetErrorState('Exception while freeing DAB-Adapter', E.Message);
      end;
      end;
    end;
  inherited Done;
end;

procedure TLazCustomDABDebugger.EmergencyStop(const Message: string);
begin
  if State in [dsInit, dsRun, dsPause, dsInternalPause] then
    SetErrorState('Debugging stopped unexpectedly', Message)
  else
    SetErrorState('The debugger stopped unexpectedly', Message);
end;

class function TLazCustomDABDebugger.CreateProperties: TDebuggerProperties;
begin
  Result := TLazDABCustomProperties.Create;
end;

procedure TLazCustomDABDebugger.OnLogCommunication(const Message: string);
begin
  if Assigned(EventLogHandler) and TLazDABCustomProperties(GetProperties).ShowDABCommunicationInEventLog then
    EventLogHandler.LogCustomEvent(ecOutput, etOutputDebugString, Message)
end;

function TLazCustomDABDebugger.RequestVariables(const AFrameId: Integer; const ARef: PtrInt; const ACallBack: TLazDebugExtensionVariablesCallback): Boolean;
var
  Command: TLazDABScopesCommand;
begin
  Command := TLazDABScopesCommand.Create(FCommunicationThread, Self, FCurrentStackFrame, ARef, ACallback);
  RunCommand(Command);
  Result := True;
end;

function TLazCustomDABDebugger.CreateDABProcess: Boolean;
begin
  Result := False;
  if FDABProcess = nil then
    begin
    FDABProcess := TProcessUTF8.Create(nil);
    try
      FDABProcess.ParseCmdLine(ExternalDebugger);
      FDABProcess.Options:= [poUsePipes, {$IF DECLARED(poDetached)}poDetached{$ELSE}poNoConsole{$ENDIF}, poStdErrToOutPut, poNewProcessGroup];
      {$if defined(windows) and not defined(wince)}
      // under win9x and winMe should be created with console,
      // otherwise no break can be sent.
      if Win32MajorVersion <= 4 then
        FDABProcess.Options:= [poUsePipes, poNewConsole, poStdErrToOutPut, poNewProcessGroup];
      {$endif windows}
      FDABProcess.ShowWindow := swoNone;
      FDABProcess.Environment:=DebuggerEnvironment;
      FDABProcess.PipeBufferSize:=64*1024;
      // Descendants may have to adapt the process-options. (i.e. add parameters)
      SetAdditionalDABProcessOptions(FDABProcess);
    except
      FreeAndNil(FDABProcess);
    end;
    end;
  if FDABProcess = nil then
    exit;

  if not FDABProcess.Running then
    begin
    try
      FDABProcess.Execute;
      Result := FDABProcess.Running;
      if GetDABConnectionType = dctTcpIp then
        // An ugly hack to give the DAB-Adapter some (arbritrary) time to bind
        // to a tcp/ip port
        sleep(500);
    except
      on E: Exception do
        SetErrorState('Exception while executing the DAB-Adapter', E.Message);
    end;
    end;
end;

function TLazCustomDABDebugger.CreateThreads: TThreadsSupplier;
begin
  Result := TLazDABThreadsSupplier.Create(Self);
end;

function TLazCustomDABDebugger.RequestThreads(const ACallBack: TLazDABThreadsCallback): Integer;
var
  Command: TLazDABThreadsCommand;
begin
  Command := TLazDABThreadsCommand.Create(FCommunicationThread, Self, ACallBack);
  RunCommand(Command);
  Result := Command.Seq;
end;

function TLazCustomDABDebugger.CreateAttachOrLaunchCommand(const AParams: array of const; const ACallback: TMethod): TLazDABCommand;
begin
  Result := TLazDABRunCommand.Create(FCommunicationThread, Self, FileName, Arguments, WorkingDir, Environment);
end;

function TLazCustomDABDebugger.GetDABConnectionType: TLazDABConnectionType;
begin
  Result := dctTcpIp;
end;

procedure TLazCustomDABDebugger.GetTcpIpProperties(out AHost: string; out APort: Integer);
begin
  AHost := '';
  APort := 0;
end;

function TLazCustomDABDebugger.GetMustCreateDABProcess: Boolean;
begin
  Result := False;
end;

procedure TLazCustomDABDebugger.SetAdditionalDABProcessOptions(AProcess: TProcessUTF8);
begin
  // Do nothing.
end;

end.

