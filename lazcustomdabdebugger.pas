{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit LazCustomDABDebugger;

{$warn 5023 off : no warning about unused units}
interface

uses
  LazDABCommunication, LazDABCommand, LazDabGlue, LazDABCustomProperties, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('LazCustomDABDebugger', @Register);
end.
