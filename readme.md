# LazDABDebugger

A debugger for Lazarus that connects to a _Debug Adapter_ to do the actual debugging, using Microsoft's Debug Adapter Protocol ([DAB](https://microsoft.github.io/debug-adapter-protocol)).
It uses the Lazarus Debug extensions, also included inside this repository, for improved variable-evaluation insize Lazarus.
At this moment is is more a proof-of-concept, only tested with the [fpdebug](https://gitlab.com/jvdsluis/fpdserver) DAB-adapter.

## Getting started

These instructions will explain how to compile, install and use this debug-server.

### Prerequisites

You'll need a DAB-adapter, like FPDebug. To be able to debug the debug-adapter must be running and listening on port 9159. When you are using FPDebug, it means it has to be started with the `-t` option.

To compile LazDABDebugger you need [Free Pascal](https://www.freepascal.org/) version 3.2 or above, [Lazarus](https://www.lazarus-ide.org/) and the [fppkg](https://wiki.lazarus.freepascal.org/fppkg) package-manager.

### Installing

Start by installing the dependencies [cerialization](https://gitlab.com/fpcprojects/cerialization) and [fpcdab](https://gitlab.com/fpcprojects/fpcdab) from the central fppkg-repository. This can be done on a command-line with these two commands:

    fppkg install cerialization
    fppkg install fpcdab

Now open the `lazdebugextensionintf.lpk` package in Lazarus and install it. Do the same with the `lazdabdebugger.lpk` package.

Now you should re-compile Lazarus and once that is done, restart the IDE.

### Basic usage

Select the DAB-Debugger as the debugging backend. This is done in tools->options (ctrl-shigt-o), `Debugger`->`Debugger backend`. Select `add` and select `DAB Debugger` as debugger type. Give it a name you can remember.

Now start debugging, using the DAB-debugger backend

## Variable evaluation

Use the evaluation-window from the Lazarus Debug Extensions (`Chow`->`Debug`->`Evaluation tree`).

## Versioning

This package is very new and receives a lot of updates. For the versions available, see the tags on this repository.

## Authors

* **Joost van der Sluis** - *initial work* - <joost@cnoc.nl>

## License

This library is distributed under the GNU General Public License version 2 (see the [COPYING.GPL.txt](COPYING.GPL.txt) file) with the following modification:

- object files and libraries linked into an application may be distributed without source code.