{ The communication-layer for the DAB-protocol

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}

unit LazDABCommunication;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  ssockets,
  Sockets,
  Forms,
  csModel,
  csJSONRttiStreamHelper,
  DABMessages,
  DbgIntfDebuggerBase,
  LazLogger;

type

  TLazDABResponseEvent = procedure (const Response: TDABResponse) of object;
  TLazDABLogEvent = procedure(const Message: string) of object;
  TLazDABEventEvent = procedure(const Event: TDABEvent) of object;


  { TLazDABCommunicationThread }

  TLazDABCommunicationThread = class(TThread)
  private
    FOnResponse: TLazDABResponseEvent;
    FOnEvent: TLazDABEventEvent;
    FOnLog: TLazDABLogEvent;
    FOnErrorLog: TLazDABLogEvent;
    FOnStop: TLazDABLogEvent;
    FConnectionClosedIntentionally: Boolean;
    FOnCommunicationLog: TLazDABLogEvent;
    procedure DoOnResponse(Data: PtrInt);
    procedure DoOnEvent(Data: PtrInt);
    procedure DoOnLog(Data: PtrInt);
    procedure DoOnLogCommunication(Data: PtrInt);
    procedure DoOnLogError(Data: PtrInt);
    procedure DoOnStop(Data: PtrInt);
  protected
    FClient: THandleStream;
    FWriteClient: THandleStream;
    FSerializerOutsideThread: TJSONRttiStreamClass;
    FSerializerInsideThread: TJSONRttiStreamClass;
    procedure ReceivedResponse(const s: string);
    procedure Execute; override;
    procedure Log(const Message: string);
    procedure LogCommunication(const Message: string);
    procedure LogError(const Message: string);
    procedure Stop(const Reason: string);
  public
    constructor Create(
      AClient: THandleStream;
      AWriteClient: THandleStream;
      AnOnResponse: TLazDABResponseEvent;
      AnOnEvent: TLazDABEventEvent;
      ALogEvent,
      ACommunicationLogEvent,
      AnErrorLogEvent,
      AStopEvent: TLazDABLogEvent);
    destructor Destroy; override;
    function SendRequest(ARequest: TDABRequest): Boolean;
    procedure CloseConnection;
    property OnResponse: TLazDABResponseEvent read FOnResponse;
    property OnEvent: TLazDABEventEvent read FOnEvent;
    property OnLog: TLazDABLogEvent read FOnLog;
    property OnCommunicationLog: TLazDABLogEvent read FOnCommunicationLog;
    property OnErrorLog: TLazDABLogEvent read FOnErrorLog;
    // This event called when for some reason the DAB-Adapter has stopped. (Can
    // be used to notify the IDE)
    property OnStop: TLazDABLogEvent read FOnStop;
  end;


implementation

type
  TDABHeader = record
    ContentLength: Integer;
  end;

{ TLazDABCommunicationThread }

constructor TLazDABCommunicationThread.Create(
  AClient: THandleStream;
  AWriteClient: THandleStream;
  AnOnResponse: TLazDABResponseEvent;
  AnOnEvent: TLazDABEventEvent;
  ALogEvent,
  ACommunicationLogEvent,
  AnErrorLogEvent,
  AStopEvent: TLazDABLogEvent);
begin
  inherited Create(False, DefaultStackSize);
  FSerializerOutsideThread := TJSONRttiStreamClass.Create;
  FSerializerOutsideThread.Describer.DefaultImportNameStyle := tcsinsCaseInsensitive;
  FSerializerOutsideThread.Describer.DefaultExportNameStyle := tcsensLowerCaseFirstChar;
  FSerializerOutsideThread.Describer.Flags := [tcsdfCreateClassInstances];
  FSerializerInsideThread := TJSONRttiStreamClass.Create;
  FSerializerInsideThread.Describer.DefaultImportNameStyle := tcsinsCaseInsensitive;
  FSerializerInsideThread.Describer.DefaultExportNameStyle := tcsensLowerCaseFirstChar;
  FSerializerInsideThread.Describer.Flags := [tcsdfCreateClassInstances];
  FClient := AClient;
  FWriteClient := AWriteClient;
  FOnResponse := AnOnResponse;
  FOnEvent := AnOnEvent;
  FOnLog := ALogEvent;
  FOnErrorLog := AnErrorLogEvent;
  FOnStop := AStopEvent;
  FOnCommunicationLog := ACommunicationLogEvent;
end;

procedure TLazDABCommunicationThread.Execute;

  function ParseHeader(const s: string): TDABHeader;
  var
    Strings: TStrings;
  begin
    Strings := TStringList.Create;
    try
      Strings.NameValueSeparator := ':';
      Strings.Text := s;
      Result.ContentLength:=StrToIntDef(Trim(Strings.Values['Content-Length']), -1);
    finally
      Strings.Free;;
    end;
  end;

const
  InputBufferSize = 1024;
var
  s: ansistring;
  i: integer;
  InputBuffer: array[0..InputBufferSize-1] of char;
  InputStr: ansistring;
  Header: TDABHeader;
begin
  try
    s := '';

    InputStr := '';
    while not terminated do
      begin
      i := FClient.Read(InputBuffer[0], InputBufferSize);
      if i > 0 then
        begin
        setlength(s,i);
        move(InputBuffer[0],s[1],i);
        InputStr:=InputStr+s;
        i := pos(#13#10#13#10, InputStr);
        while (i > 0) and not Terminated do
          begin
          s := copy(InputStr, 1, i-1);
          delete(InputStr, 1, i+3);
          Header := ParseHeader(S);
          if Header.ContentLength<0 then
            begin
            Stop(Format('Could not parse DAB-header. Terminate connection. Buffer-contents [%s]', [s]));
            Terminate;
            end;
          i := length(InputStr);
          if i < Header.ContentLength then
            begin
            SetLength(InputStr, Header.ContentLength);
            FClient.ReadBuffer(InputStr[i+1], Header.ContentLength-i);
            end;
          s := copy(InputStr, 1, Header.ContentLength);
          try
            ReceivedResponse(s);
          except
            // Maybe it would be better to ask the user if we should continue or
            // not. Depends on the kind of problems that are encountered.
            on E: Exception do
              LogError(Format('Exception while handling DAB message: [%s] - [%s]', [s, E.Message]));
          end;
          Delete(InputStr, 1, Header.ContentLength);
          i := pos(#13#10, InputStr);
          end;
        end
      else if (i < 0) and not Terminated then
        begin
        if FClient is TInetSocket then
          Stop(Format('Error during read. Socket-error: [%d]', [TInetSocket(FClient).LastError]))
        else
          Stop('Error during read.');
        Terminate;
        end
      else if i = 0 then
        begin
        // Zero-count -> Connection closed
        Stop('Lost connection with the The DAB-adapter.');
        Terminate;
        end;
      end;
  except
    on E: Exception do
      Stop(Format('Exception in DAB-communication: [%s]', [E.Message]));
  end;
  Log('DAB Communication channel closed.');
end;

function TLazDABCommunicationThread.SendRequest(ARequest: TDABRequest): Boolean;

  function AddHeader(const AStr: UTF8String): UTF8String;
  begin
    Result := 'Content-Length: ' + IntToStr(Length(AStr)) + #13#10#13#10 + AStr;
  end;

var
  SendStr: UTF8String;
begin
  SendStr := FSerializerOutsideThread.ObjectToJSONString(ARequest);
  LogCommunication('DAB Send: '+SendStr);
  SendStr := AddHeader(SendStr);
  FWriteClient.WriteBuffer(SendStr[1], Length(SendStr))
end;

destructor TLazDABCommunicationThread.Destroy;
begin
  FSerializerInsideThread.Free;
  FSerializerOutsideThread.Free;
  inherited Destroy;
end;

procedure TLazDABCommunicationThread.CloseConnection;
begin
  FConnectionClosedIntentionally := True;
  fpshutdown(FClient.Handle, SHUT_RDWR);
end;

procedure TLazDABCommunicationThread.ReceivedResponse(const s: string);
var
  Response: TDABResponse;
  ProtMessage: TDABProtocolMessage;
  Event: TDABEvent;
begin
  LogCommunication('DAB Received: '+s);
  ProtMessage := TDABProtocolMessage.create;
  try
    try
      FSerializerInsideThread.JSONStringToObject(s, ProtMessage);
    except
      // When the incoming message is not even a valid protocol-message, log
      // a complaint to the user.
      on E: Exception do
        begin
        LogError(Format('Invalid DAB message received: [%s] - [%s]', [s, E.Message]));
        Exit;
        end;
    end;
    case ProtMessage.&Type of
      'response':
        begin
        if Assigned(OnResponse) then
          begin
          Response := TDABResponse.create;
          FSerializerInsideThread.JSONStringToObject(s, Response);
          //Response := FSerializerInsideThread.CreateObjectFromJSONString<TDABResponse>(s);
          case Response.Command of
            'setBreakpoints':
              begin
              Response.Free;
              Response := TDABBreakpointsResponse.create;
              FSerializerInsideThread.JSONStringToObject(s, Response);
              end;
            'stackTrace':
              begin
              Response.Free;
              Response := TDABStackTraceResponse.create;
              FSerializerInsideThread.JSONStringToObject(s, Response);
              end;
            'evaluate':
              begin
              Response.Free;
              Response := TDABEvaluateResponse.create;
              FSerializerInsideThread.JSONStringToObject(s, Response);
              end;
            'variables':
              begin
              Response.Free;
              Response := TDABVariablesResponse.create;
              FSerializerInsideThread.JSONStringToObject(s, Response);
              end;
            'scopes':
              begin
              Response.Free;
              Response := TDABScopesResponse.create;
              FSerializerInsideThread.JSONStringToObject(s, Response);
              end;
            'threads':
              begin
              Response.Free;
              Response := TDABThreadsResponse.create;
              FSerializerInsideThread.JSONStringToObject(s, Response);
              end;
          end; {case}
          Application.QueueAsyncCall(@DoOnResponse, PtrInt(Response));
          end;
        end;
      'event':
        begin
        if Assigned(OnEvent) then
          begin
          Event := TDABEvent.create;
          FSerializerInsideThread.JSONStringToObject(s, Event);

          case Event.Event of
            'output':
              begin
              Event.Free;
              Event := TDABOutputEvent.create;
              FSerializerInsideThread.JSONStringToObject(s, Event);
              end;
            'stopped':
              begin
              Event.Free;
              Event := TDABStoppedEvent.create;
              FSerializerInsideThread.JSONStringToObject(s, Event);
              end;
          end; {case}
          Application.QueueAsyncCall(@DoOnEvent, PtrInt(Event));
          end;
        end;
      else
        begin
        LogError(Format('Received unsupported DAB message: [%s]', [s]));
        end;
    end;

  finally
    ProtMessage.Free;
  end;
end;

procedure TLazDABCommunicationThread.DoOnResponse(Data: PtrInt);
begin
  OnResponse(TObject(Data) as TDABResponse);
end;

procedure TLazDABCommunicationThread.DoOnEvent(Data: PtrInt);
begin
  OnEvent(TObject(Data) as TDABEvent);
end;

procedure TLazDABCommunicationThread.DoOnLog(Data: PtrInt);
var
  Msg: string;
begin
  Msg := StrPas(PChar(Data));
  OnLog(Msg);
  StrDispose(PChar(Data));
end;

procedure TLazDABCommunicationThread.DoOnLogError(Data: PtrInt);
var
  Msg: string;
begin
  Msg := StrPas(PChar(Data));
  OnErrorLog(Msg);
  StrDispose(PChar(Data));
end;

procedure TLazDABCommunicationThread.Log(const Message: string);
var
  p: PChar;
begin
  // When connecting to the DAB-adapter fails, the thread is started but exited
  // and freed immediately. There is no way any log-messages can come through
  // safely. So FOnLog is only set once the connection has been setup, so we
  // can skip sending log-messages in this case.
  if Assigned(FOnLog) then
    begin
    p := StrAlloc(Length(Message)+1);
    StrPCopy(p, Message);
    Application.QueueAsyncCall(@DoOnLog, PtrInt(p));
    end;
end;

procedure TLazDABCommunicationThread.LogError(const Message: string);
var
  p: PChar;
begin
  p := StrAlloc(Length(Message)+1);
  StrPCopy(p, Message);
  Application.QueueAsyncCall(@DoOnLog, PtrInt(p));
end;

procedure TLazDABCommunicationThread.DoOnStop(Data: PtrInt);
var
  Msg: string;
begin
  Msg := StrPas(PChar(Data));
  // If the connection was closed intentionally, do not show any warning to
  // the user.
  if not FConnectionClosedIntentionally then
    OnStop(Msg);
  StrDispose(PChar(Data));
end;

procedure TLazDABCommunicationThread.Stop(const Reason: string);
var
  p: PChar;
begin
  p := StrAlloc(Length(Reason)+1);
  StrPCopy(p, Reason);
  Application.QueueAsyncCall(@DoOnStop, PtrInt(p));
end;

procedure TLazDABCommunicationThread.LogCommunication(const Message: string);
var
  p: PChar;
begin
  if Assigned(FOnCommunicationLog) then
    begin
    p := StrAlloc(Length(Message)+1);
    StrPCopy(p, Message);
    Application.QueueAsyncCall(@DoOnLogCommunication, PtrInt(p));
    end;
end;

procedure TLazDABCommunicationThread.DoOnLogCommunication(Data: PtrInt);
var
  Msg: string;
begin
  Msg := StrPas(PChar(Data));
  OnCommunicationLog(Msg);
  StrDispose(PChar(Data));
end;

end.

