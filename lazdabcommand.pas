{ Debug-commands that implement the debug-logic for the DAB-protocol

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDABCommand;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  process,
  Generics.Collections,
  DbgIntfDebuggerBase,
  DbgIntfBaseTypes,
  DABMessages,
  LazDebuggerIntf,
  LazDebuggerIntfBaseTypes,
  LazDabGlue,
  LazDebugExtensionDebuggerIntf,
  LazDABCommunication,
  LazDABCustomDebugger;

type
  TLazDABBreakpointArray = array of TDBGBreakPoint;

  { TLazDABInitializeCommand }

  TLazDABInitializeCommand = class(TLazDABCommand)
  public
    procedure Execute(out IsFinished: Boolean); override;
  end;

  { TLazDABConfigurationDoneCommand }

  TLazDABConfigurationDoneCommand = class(TLazDABCommand)
  public
    procedure Execute(out IsFinished: Boolean); override;
  end;


  { TLazDABRunCommand }

  TLazDABRunCommand = class(TLazDABCommand)
  protected
    FFileName: string;
    FWorkingDir: string;
    FArguments: TStringArray;
    FEnvironment: TStrings;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AFileName, AnArguments, AWorkingDir: string; const AnEnvironment: TStrings);
    destructor Destroy; override;
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABAttachCommand }

  TLazDABAttachCommand = class(TLazDABCommand)
  public
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABContinueCommand }

  TLazDABContinueCommand = class(TLazDABCommand)
  private
    FContinueType: TLazDabContinueType;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AContinueType: TLazDabContinueType);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABPauseCommand }

  TLazDABPauseCommand = class(TLazDABCommand)
  public
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABDisconnectCommand }

  TLazDABDisconnectCommand = class(TLazDABCommand)
  public
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;


  { TLazDABStackTraceCommand }

  TLazDABStackTraceCommand = class(TLazDABCommand)
  private
    FThreadId: Integer;
    FDoCurrent: Boolean;
    FCount: Integer;
    FStackList: TCallStackBase;
    procedure DoStacklistFreedNotification(Sender: TObject);
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AThreadId, ACount: Integer; AStackList: TCallStackBase; ADoCurrent: Boolean);
    destructor Destroy; override;
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABSetBreakpointsCommand }

  TLazDABSetBreakpointsCommand = class(TLazDABCommand)
  protected
    FSource: string;
    FBreakPointArray: TLazDABBreakpointArray;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; ASource: string; ABreakPointArray: TLazDABBreakpointArray);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABEvaluateCommand }

  TLazDABEvaluateCommand = class(TLazDABCommand)
  protected
    FExpression: string;
    FCallback: TDBGEvaluateResultCallback;
    FStackFrameId: Integer;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AnExpression: string; AStackFrameId: Integer; ACallback: TDBGEvaluateResultCallback);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABExtEvaluateCommand }

  TLazDABExtEvaluateCommand = class(TLazDABCommand)
  protected
    FExpression: string;
    FCallback: TLazDebugExtensionEvaluateCallback;
    FStackFrameId: Integer;
    FReference: PtrInt;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AnExpression: string; AStackFrameId: Integer; AReference: PtrInt; ACallback: TLazDebugExtensionEvaluateCallback);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABScopesCommand }

  TLazDABScopesCommand = class(TLazDABCommand)
  protected
    FStackFrameId: Integer;
    FCallback: TLazDebugExtensionVariablesCallback;
    FReference: PtrInt;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AStackFrameId: Integer; AReference: PtrInt; ACallback: TLazDebugExtensionVariablesCallback);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABExtVariablesCommand }

  TLazDABExtVariablesCommand = class(TLazDABCommand)
  private
    FVariablesReference: PtrInt;
    FCallback: TLazDebugExtensionLoadChildsCallback;
    FStart: Integer;
    FCount: Integer;
    FReference: TDBGPtr;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AVariablesReference: PtrInt; AStart, ACount: Integer; AReference: PtrInt; ACallback: TLazDebugExtensionLoadChildsCallback);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;

  { TLazDABThreadsCommand }

  TLazDABThreadsCommand = class(TLazDABCommand)
  public
    FCallback: TLazDABThreadsCallback;
  public
    constructor Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; ACallBack: TLazDABThreadsCallback);
    procedure Execute(out IsFinished: Boolean); override;
    procedure HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean); override;
  end;


implementation

{ TLazDABThreadsCommand }

procedure TLazDABThreadsCommand.Execute(out IsFinished: Boolean);
var
  ThreadsRequest: TDABRequest;
begin
  IsFinished := False;
  ThreadsRequest := TDABRequest.create;
  try
    ThreadsRequest.InitSequence;
    ThreadsRequest.Command := 'threads';
    FSeq := ThreadsRequest.Seq;
    FCommunicationThread.SendRequest(ThreadsRequest);
  finally
    ThreadsRequest.Free;
  end;
end;

procedure TLazDABThreadsCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  Threads: TDABThreadList;
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  if AResponse.Success then
    Threads := (AResponse.Body as TDABThreadsResponseBody).Threads
  else
    Threads := nil;
  FCallback(AResponse.Request_seq, AResponse.Success, AResponse.Message, Threads);
end;

constructor TLazDABThreadsCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; ACallBack: TLazDABThreadsCallback);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FCallback := ACallBack;
end;

{ TLazDABAttachCommand }

procedure TLazDABAttachCommand.Execute(out IsFinished: Boolean);
var
  AttachRequest: TDABAttachRequest;
begin
  IsFinished := False;
  AttachRequest := TDABAttachRequest.create;
  try
    AttachRequest.InitSequence;
    AttachRequest.Command := 'attach';

    FSeq := AttachRequest.Seq;
    FCommunicationThread.SendRequest(AttachRequest);

    SetState(dsInit);
  finally
    AttachRequest.Free;
  end;
end;

procedure TLazDABAttachCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
begin
  inherited HandleResponse(AResponse, AnIsFinished);

  if not AResponse.Success then
    begin
    SetState(dsStop);
    FDebugger.OnFeedback(Self, 'Failed to attach to debuggee', AResponse.Message, ftWarning, [frOk]);
    end
  else
    SetState(dsRun)
end;

{ TLazDABScopesCommand }

constructor TLazDABScopesCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AStackFrameId: Integer; AReference: PtrInt; ACallback: TLazDebugExtensionVariablesCallback);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FStackFrameId := AStackFrameId;
  FCallback := ACallback;
  FReference := AReference;
end;

procedure TLazDABScopesCommand.Execute(out IsFinished: Boolean);
var
  ScopesRequest: TDABScopesRequest;
begin
  IsFinished := False;
  ScopesRequest := TDABScopesRequest.create;
  try
    ScopesRequest.InitSequence;
    ScopesRequest.Command := 'scopes';
    ScopesRequest.Arguments.FrameId := FStackFrameId;
    FSeq := ScopesRequest.Seq;
    FCommunicationThread.SendRequest(ScopesRequest);
  finally
    ScopesRequest.Free;
  end;
end;

procedure TLazDABScopesCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  ScopesResponse: TDABScopesResponseBody;
  VariableList: TLazDebugExtensionVariableList;
  Variable: TLazDebugExtensionVariable;
  i: Integer;
begin
  inherited HandleResponse(AResponse, AnIsFinished);

  if AResponse.Success then
    begin
    ScopesResponse := AResponse.Body as TDABScopesResponseBody;

    VariableList := TLazDebugExtensionVariableList.Create(True);
    for i := 0 to ScopesResponse.Scopes.Count - 1 do
      begin
      Variable := TLazDebugExtensionVariable.Create(
        '',
        ScopesResponse.Scopes[i].Name,
        '',
        ScopesResponse.Scopes[i].VariablesReference);
      VariableList.Add(Variable);
      end;
    end
  else
    VariableList := nil;

  FCallback(FReference, AResponse.Success, AResponse.Message, VariableList);
end;

{ TLazDABDisconnectCommand }

procedure TLazDABDisconnectCommand.Execute(out IsFinished: Boolean);
var
  DisconnectRequest: TDABRequest;
begin
  IsFinished := False;
  DisconnectRequest := TDABRequest.create;
  try
    DisconnectRequest.InitSequence;
    DisconnectRequest.Command := 'disconnect';
    FSeq := DisconnectRequest.Seq;
    FCommunicationThread.SendRequest(DisconnectRequest);
  finally
    DisconnectRequest.Free;
  end;
end;

procedure TLazDABDisconnectCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  if not AResponse.Success then
    begin
    FDebugger.OnFeedback(Self, AResponse.Message, 'Failed to stop the debuggee', ftWarning, [frOk]);
    end;
end;

{ TLazDABPauseCommand }

procedure TLazDABPauseCommand.Execute(out IsFinished: Boolean);
var
  PauseRequest: TDABRequest;
begin
  IsFinished := False;
  PauseRequest := TDABRequest.create;
  try
    PauseRequest.InitSequence;
    PauseRequest.Command := 'pause';
    FSeq := PauseRequest.Seq;
    FCommunicationThread.SendRequest(PauseRequest);
  finally
    PauseRequest.Free;
  end;
end;

procedure TLazDABPauseCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  if not AResponse.Success then
    begin
    FDebugger.OnFeedback(Self, AResponse.Message, 'Failed to pause debuggee', ftWarning, [frOk]);
    end;
end;

{ TLazDABExtVariablesCommand }

constructor TLazDABExtVariablesCommand.Create(
  ACommunicationThread: TLazDABCommunicationThread;
  ADebugger: TLazCustomDABDebugger;
  AVariablesReference: PtrInt;
  AStart, ACount: Integer;
  AReference: PtrInt;
  ACallback: TLazDebugExtensionLoadChildsCallback);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FVariablesReference := AVariablesReference;
  FReference := AReference;
  FCallback := ACallback;
  FStart := AStart;
  FCount := ACount;
end;

procedure TLazDABExtVariablesCommand.Execute(out IsFinished: Boolean);
var
  VariablesRequest: TDABVariablesRequest;
begin
  IsFinished := False;
  VariablesRequest := TDABVariablesRequest.create;
  try
    VariablesRequest.InitSequence;
    VariablesRequest.Command := 'variables';
    VariablesRequest.Arguments.VariablesReference := FVariablesReference;
    VariablesRequest.Arguments.Start := FStart;
    VariablesRequest.Arguments.Count := FCount;
    FSeq := VariablesRequest.Seq;
    FCommunicationThread.SendRequest(VariablesRequest);
  finally
    VariablesRequest.Free;
  end;
end;

procedure TLazDABExtVariablesCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  VariablesResponse: TDABVariablesResponse;
  VariableListOut: TLazDebugExtensionVariableList;
  VariableListIn: TDABVariableList;
  VariableIn: TDABVariable;
  i: Integer;
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  VariablesResponse := AResponse as TDABVariablesResponse;

  if VariablesResponse.Success then
    begin
    VariableListOut := TLazDebugExtensionVariableList.Create(True);
    VariableListIn :=VariablesResponse.Body.Variables;
    for i := 0 to VariableListIn.Count -1 do
      begin
      VariableIn := VariableListIn[i];
      VariableListOut.Add(TLazDebugExtensionVariable.Create(
        VariableIn.Value,
        VariableIn.Name,
        VariableIn.&Type,
        VariableIn.VariablesReference));
      end;
    end
  else
    VariableListOut := nil;

  FCallback(FReference, VariablesResponse.Success, VariablesResponse.Message, VariableListOut);
end;

{ TLazDABExtEvaluateCommand }

constructor TLazDABExtEvaluateCommand.Create(
  ACommunicationThread: TLazDABCommunicationThread;
  ADebugger: TLazCustomDABDebugger;
  AnExpression: string;
  AStackFrameId: Integer;
  AReference: PtrInt;
  ACallback: TLazDebugExtensionEvaluateCallback);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FCallback := ACallback;
  FExpression := AnExpression;
  FStackFrameId := AStackFrameId;
  FReference := AReference;
end;

procedure TLazDABExtEvaluateCommand.Execute(out IsFinished: Boolean);
var
  EvaluateRequest: TDABEvaluateRequest;
begin
  IsFinished := False;
  EvaluateRequest := TDABEvaluateRequest.create;
  try
    EvaluateRequest.InitSequence;
    EvaluateRequest.Command := 'evaluate';
    EvaluateRequest.Arguments.Expression := FExpression;
    EvaluateRequest.Arguments.Context := 'repl';
    EvaluateRequest.Arguments.FrameId := FStackFrameId;
    FSeq := EvaluateRequest.Seq;
    FCommunicationThread.SendRequest(EvaluateRequest);
  finally
    EvaluateRequest.Free;
  end;
end;

procedure TLazDABExtEvaluateCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  EvaluateResponse: TDABEvaluateResponse;
  Variable: TLazDebugExtensionVariable;
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  EvaluateResponse := AResponse as TDABEvaluateResponse;

  if EvaluateResponse.Success then
    Variable := TLazDebugExtensionVariable.Create(
      EvaluateResponse.Body.Result,
      '',
      EvaluateResponse.Body.&Type,
      EvaluateResponse.Body.VariablesReference)
  else
    Variable := nil;

  FCallback(FReference, EvaluateResponse.Success, EvaluateResponse.Message, Variable);
end;

{ TLazDABEvaluateCommand }

constructor TLazDABEvaluateCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AnExpression: string; AStackFrameId: Integer; ACallback: TDBGEvaluateResultCallback);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FCallback := ACallback;
  FExpression := AnExpression;
  FStackFrameId := AStackFrameId;
end;

procedure TLazDABEvaluateCommand.Execute(out IsFinished: Boolean);
var
  EvaluateRequest: TDABEvaluateRequest;
begin
  IsFinished := False;
  EvaluateRequest := TDABEvaluateRequest.create;
  try
    EvaluateRequest.InitSequence;
    EvaluateRequest.Command := 'evaluate';
    EvaluateRequest.Arguments.Expression := FExpression;
    EvaluateRequest.Arguments.Context := 'repl';
    EvaluateRequest.Arguments.FrameId := FStackFrameId;
    FSeq := EvaluateRequest.Seq;
    FCommunicationThread.SendRequest(EvaluateRequest);
  finally
    EvaluateRequest.Free;
  end;
end;

procedure TLazDABEvaluateCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  EvaluateResponse: TDABEvaluateResponse;
  RespMessage: string;
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  EvaluateResponse := AResponse as TDABEvaluateResponse;
  if not EvaluateResponse.Success then
    RespMessage := EvaluateResponse.Message
  else
    RespMessage := EvaluateResponse.Body.Result;

  FCallback(Self, EvaluateResponse.Success, RespMessage, TDBGType.Create(skNone, EvaluateResponse.Body.&Type));
end;

{ TLazDABStackTraceCommand }

constructor TLazDABStackTraceCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AThreadId, ACount: Integer; AStackList: TCallStackBase; ADoCurrent: Boolean);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FThreadId := AThreadId;
  FDoCurrent := ADoCurrent;
  FCount := ACount;
  FStackList := AStackList;

  FStackList.AddFreeNotification(@DoStacklistFreedNotification);
end;

procedure TLazDABStackTraceCommand.Execute(out IsFinished: Boolean);
var
  StacktraceRequest: TDABStackTraceRequest;
begin
  IsFinished := True;
  if not Assigned(FStackList) then
    Exit;

  IsFinished := False;
  StacktraceRequest := TDABStackTraceRequest.create;
  try
    StacktraceRequest.InitSequence;
    StacktraceRequest.Command := 'stackTrace';
    StacktraceRequest.Arguments.ThreadId := FThreadId;
    StacktraceRequest.Arguments.Levels := FCount;
    FSeq := StacktraceRequest.Seq;
    FCommunicationThread.SendRequest(StacktraceRequest);
  finally
    StacktraceRequest.Free;
  end;
end;

procedure TLazDABStackTraceCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  LocRec: TDBGLocationRec;
  StackTraceResponse: TDABStackTraceResponse;
  StackFrames: TDABStackFrameList;
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  StackTraceResponse := AResponse as TDABStackTraceResponse;
  if FDoCurrent then
    begin
    StackFrames := StackTraceResponse.Body.StackFrames;
    if StackFrames.Count > 0 then
      begin
      LocRec.Address := 0;
      LocRec.FuncName := StackFrames[0].Name;
      if Assigned(StackFrames[0].Source) then
        begin
        LocRec.SrcFile := StackFrames[0].Source.Name;
        LocRec.SrcFullName := StackFrames[0].Source.Path;
        end;
      LocRec.SrcLine := StackFrames[0].Line;
      FDebugger.SetCurrentLocation(LocRec, StackFrames[0].Id);
      end
    else
      FDebugger.OnFeedback(Self, 'Execution of the debuggee is stopped, but the location is unknown', 'No stackframes returned', ftInformation, [frOk]);
    end;
  if Assigned(FStackList) then
    (FDebugger.CallStack as TLazDABCallStackSupplier).HandleCallstackResponse(FThreadId, FStackList, StackTraceResponse.Body);
end;

procedure TLazDABStackTraceCommand.DoStacklistFreedNotification(Sender: TObject);
begin
  FStackList := nil;
end;

destructor TLazDABStackTraceCommand.Destroy;
begin
  if Assigned(FStackList) then
    FStackList.RemoveFreeNotification(@DoStacklistFreedNotification);
  inherited Destroy;
end;

{ TLazDABContinueCommand }

procedure TLazDABContinueCommand.Execute(out IsFinished: Boolean);
var
  ContinueRequest: TDABRequest;
begin
  IsFinished := False;
  ContinueRequest := TDABRequest.create;
  try
    ContinueRequest.InitSequence;
    case FContinueType of
      ctContinue: ContinueRequest.Command := 'continue';
      ctNext: ContinueRequest.Command := 'next';
      ctStepIn: ContinueRequest.Command := 'stepIn';
      ctStepOut: ContinueRequest.Command := 'stepOut';
    end;

    FSeq := ContinueRequest.Seq;
    FCommunicationThread.SendRequest(ContinueRequest);
    // Set the state to dsRun already. I don't know if this is the smart thing
    // to do, though.
    SetState(dsRun);
  finally
    ContinueRequest.Free;
  end;
end;

procedure TLazDABContinueCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  if not AResponse.Success then
    begin
    // State has already been set to dsRun (in Execute), so revert it to dsPause.
    SetState(dsPause);
    FDebugger.OnFeedback(Self, AResponse.Message, 'Failed to continue debuggee', ftWarning, [frOk]);
    end;
end;

constructor TLazDABContinueCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AContinueType: TLazDabContinueType);
begin
  Inherited Create(ACommunicationThread, ADebugger);
  FContinueType := AContinueType;
end;

{ TLazDABSetBreakpointsCommand }

constructor TLazDABSetBreakpointsCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; ASource: string; ABreakPointArray: TLazDABBreakpointArray);
begin
  inherited Create(ACommunicationThread, ADebugger);
  FSource := ASource;
  FBreakPointArray := ABreakPointArray;
end;

procedure TLazDABSetBreakpointsCommand.Execute(out IsFinished: Boolean);
var
  BreakpointRequest: TDABSetBreakpointsRequest;
  SourceBPL: TDABSourceBreakpoint;
  i: Integer;
begin
  IsFinished := False;
  BreakpointRequest := TDABSetBreakpointsRequest.create;
  try
    BreakpointRequest.InitSequence;
    BreakpointRequest.Command := 'setBreakpoints';
    FSeq := BreakpointRequest.Seq;

    BreakpointRequest.Arguments.Source.Name := ExtractFileName(FSource);
    BreakpointRequest.Arguments.Source.Path := FSource;
    for i := 0 to High(FBreakPointArray) do
      begin
      SourceBPL := TDABSourceBreakpoint.Create;
      SourceBPL.Line := FBreakPointArray[i].Line;
      BreakpointRequest.Arguments.Breakpoints.Add(SourceBPL);
      end;
    FCommunicationThread.SendRequest(BreakpointRequest);
  finally
    BreakpointRequest.Free;
  end;
end;

procedure TLazDABSetBreakpointsCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
var
  ResponseBreakpointList: TDABBreakpointList;
  LazBreakpoint: TLazDabBreakpoint;
  i, j: Integer;
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  ResponseBreakpointList := (AResponse as TDABBreakpointsResponse).Body.Breakpoints;
  for i := 0 to ResponseBreakpointList.Count -1 do
    begin
    for j := 0 to High(FBreakPointArray) do
      begin
      if FBreakPointArray[j].Line=ResponseBreakpointList[i].Line then
        begin
        LazBreakpoint := (FBreakPointArray[j] as TLazDabBreakpoint);
        LazBreakpoint.DABID := ResponseBreakpointList[i].Id;
        if not ResponseBreakpointList[i].Verified then
          LazBreakpoint.BPSetValid(vsInvalid);
        Break;
        end;
      end;
    end;
end;

{ TLazDABConfigurationDoneCommand }

procedure TLazDABConfigurationDoneCommand.Execute(out IsFinished: Boolean);
var
  ConfigurationDoneRequest: TDABRequest;
begin
  IsFinished := False;
  ConfigurationDoneRequest := TDABRequest.create;
  try
    ConfigurationDoneRequest.InitSequence;
    ConfigurationDoneRequest.Command := 'configurationDone';
    FSeq := ConfigurationDoneRequest.Seq;
    FCommunicationThread.SendRequest(ConfigurationDoneRequest);
  finally
    ConfigurationDoneRequest.Free;
  end;
end;

{ TLazDABRunCommand }

procedure TLazDABRunCommand.Execute(out IsFinished: Boolean);
var
  LaunchRequest: TDABLaunchRequest;
  i: Integer;
begin
  IsFinished := False;
  LaunchRequest := TDABLaunchRequest.create;
  try
    LaunchRequest.InitSequence;
    LaunchRequest.Command := 'launch';
    LaunchRequest.Arguments.&Program := FFileName;
    LaunchRequest.Arguments.Parameters := FArguments;
    LaunchRequest.Arguments.WorkingDirectory := FWorkingDir;

    for i := 0 to FEnvironment.Count -1 do
      LaunchRequest.Arguments.Environment.Add(TDABLaunchRequestArgumentEnvironmentVariable.Create(FEnvironment.Names[i], FEnvironment.ValueFromIndex[i]));

    FSeq := LaunchRequest.Seq;
    FCommunicationThread.SendRequest(LaunchRequest);

    SetState(dsInit);
  finally
    LaunchRequest.Free;
  end;
end;

constructor TLazDABRunCommand.Create(ACommunicationThread: TLazDABCommunicationThread; ADebugger: TLazCustomDABDebugger; AFileName, AnArguments, AWorkingDir: string; const AnEnvironment: TStrings);
var
  List: TProcessStrings;
begin
  inherited Create(ACommunicationThread, ADebugger);
  FFileName := AFileName;
  FWorkingDir := AWorkingDir;
  FEnvironment := TStringList.Create;
  FEnvironment.Assign(AnEnvironment);
  List := TProcessStringList.Create;
  try
    CommandToList(AnArguments, List);
    FArguments := List.ToStringArray();
  finally
    List.Free;
  end;
end;

procedure TLazDABRunCommand.HandleResponse(const AResponse: TDABResponse; out AnIsFinished: Boolean);
begin
  inherited HandleResponse(AResponse, AnIsFinished);
  if not AResponse.Success then
    begin
    SetState(dsStop);
    FDebugger.OnFeedback(Self, 'Failed to launch debuggee', AResponse.Message, ftWarning, [frOk]);
    end
  else
    SetState(dsRun)
end;

destructor TLazDABRunCommand.Destroy;
begin
  FEnvironment.Free;
  inherited Destroy;
end;

{ TLazDABInitializeCommand }

procedure TLazDABInitializeCommand.Execute(out IsFinished: Boolean);
var
  InitializeRequest: TDABInitializeRequest;
begin
  IsFinished := False;
  InitializeRequest := TDABInitializeRequest.create;
  try
    InitializeRequest.InitSequence;
    InitializeRequest.Command := 'initialize';
    InitializeRequest.Arguments.AdapterID := 'CustomAdapter';
    FSeq := InitializeRequest.Seq;
    FCommunicationThread.SendRequest(InitializeRequest);
  finally
    InitializeRequest.Free;
  end;
end;

end.

