unit LazDABProperties;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  DbgIntfDebuggerBase,
  LazDABCustomProperties,
  LazDABCustomDebugger;

type
  TDABConnect = (dabLaunch, dabAttach);

  { TLazDABProperties }

  TLazDABProperties = class(TLazDABCustomProperties)
  private
    FDABAdapterHostname: string;
    FDABAdapterPort: integer;
    FDABConnect: TDABConnect;
    FRunDABAdapter: Boolean;
    FDABConnectionType: TLazDABConnectionType;
    FDABAdapterParameters: string;
    FDABAdapterRunDirectory: string;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property DABAdapterHostname: string read FDABAdapterHostname write FDABAdapterHostname;
    property DABAdapterPort: Integer read FDABAdapterPort write FDABAdapterPort;
    property DABAdapterParameters: string read FDABAdapterParameters write FDABAdapterParameters;
    property DABAdapterRunDirectory: string read FDABAdapterRunDirectory write FDABAdapterRunDirectory;
    property DABConnect: TDABConnect read FDABConnect write FDABConnect;
    property RunDABAdapter: Boolean read FRunDABAdapter write FRunDABAdapter;
    property DABConnectionType: TLazDABConnectionType read FDABConnectionType write FDABConnectionType;
  end;


implementation

{ TLazDABProperties }

procedure TLazDABProperties.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TLazDABProperties then
    begin
    FDABAdapterHostname := TLazDABProperties(Source).DABAdapterHostname;
    FDABAdapterPort := TLazDABProperties(Source).DABAdapterPort;
    FDABConnect := TLazDABProperties(Source).DABConnect;
    FRunDABAdapter := TLazDABProperties(Source).RunDABAdapter;
    FDABConnectionType := TLazDABProperties(Source).DABConnectionType;
    FDABAdapterParameters := TLazDABProperties(Source).DABAdapterParameters;
    FDABAdapterRunDirectory := TLazDABProperties(Source).DABAdapterRunDirectory;
    end;
end;

end.

