{ Implementation of the debugging interfaces to communicate with the Lazarus IDE

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDABDebuggerRegistration;

interface

uses
  Classes,
  SysUtils,
  fpjson,
  Generics.Collections,
  process,
  UTF8Process,
  DbgIntfDebuggerBase,
  LazFileUtils,
  LazDebugExtensionDebuggerIntf,
  LazDABCustomDebugger,
  LazDabGlue,
  LazDABCommunication,
  LazDABProperties,
  LazDABCommand,
  Forms;

type

  { TLazDABDebugger }

  TLazDABDebugger = class(TLazCustomDABDebugger)
  protected
    function CreateAttachOrLaunchCommand(const AParams: array of const; const ACallback: TMethod): TLazDABCommand; override;

  protected
    function GetDABConnectionType: TLazDABConnectionType; override;
    function GetMustCreateDABProcess: Boolean; override;
    procedure GetTcpIpProperties(out AHost: string; out APort: Integer); override;
    procedure SetAdditionalDABProcessOptions(AProcess: TProcessUTF8); override;
  public
    class function CreateProperties: TDebuggerProperties; override;
    class function Caption: String; override;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterDebugger(TLazDABDebugger);
end;

{ TLazDABDebugger }

class function TLazDABDebugger.Caption: string;
begin
  Result := 'Generic DAB Debugger';
end;

class function TLazDABDebugger.CreateProperties: TDebuggerProperties;
begin
  Result := TLazDABProperties.Create;
end;

function TLazDABDebugger.CreateAttachOrLaunchCommand(const AParams: array of const; const ACallback: TMethod): TLazDABCommand;
begin
  case TLazDABProperties(GetProperties).DABConnect of
    dabAttach: Result := TLazDABAttachCommand.Create(FCommunicationThread, Self);
    dabLaunch: Result := TLazDABRunCommand.Create(FCommunicationThread, Self, FileName, Arguments, WorkingDir, Environment);
  end;
end;

function TLazDABDebugger.GetDABConnectionType: TLazDABConnectionType;
begin
  Result := TLazDABProperties(GetProperties).DABConnectionType;
end;

function TLazDABDebugger.GetMustCreateDABProcess: Boolean;
begin
  Result := TLazDABProperties(GetProperties).RunDABAdapter;
end;

procedure TLazDABDebugger.GetTcpIpProperties(out AHost: string; out APort: Integer);
begin
  AHost := TLazDABProperties(GetProperties).DABAdapterHostname;
  APort := TLazDABProperties(GetProperties).DABAdapterPort;
end;

procedure TLazDABDebugger.SetAdditionalDABProcessOptions(AProcess: TProcessUTF8);
var
  ParamList: TStrings;
begin
  ParamList := TStringList.Create;
  try
    SplitCmdLineParams(TLazDABProperties(GetProperties).DABAdapterParameters, ParamList);
    AProcess.Parameters.AddStrings(ParamList);
  finally
    ParamList.Free;
  end;
  AProcess.CurrentDirectory := TLazDABProperties(GetProperties).DABAdapterRunDirectory;
end;

end.

