{ Additional locals-dialog, which shows the data in a tree-like manner.

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDebugExtensionVariablesDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  MenuIntf,
  laz.VirtualTrees,
  Dialogs,
  IDEWindowIntf,
  LazDebugExtensionStringTreeController,
  LazDebugExtensionDebuggerIntf;

type

  { TDebugExtensionVariablesDlg }

  TDebugExtensionVariablesDlg = class(TForm)
    vstVariables: TLazVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormDestroy(Sender: TObject);
  private
    FLatestRef: PtrInt;
    class var FInstance: TDebugExtensionVariablesDlg;
    class function GetInstance: TDebugExtensionVariablesDlg; static;
    procedure HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent);
    procedure RequestCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariableList: TLazDebugExtensionVariableList);
  protected
    FController: TLazDebugExtensionStringTreeController;
    procedure RefreshScopes;
  public
    class property Instance: TDebugExtensionVariablesDlg read GetInstance;
  end;

procedure Register;

implementation

{$R *.lfm}

resourcestring
  sVariables = 'Scopes';
  sVariablesCaption = 'Tree-based scopes';
  sNoDebugger = 'No debugger available';
  sRequestVariablesCallFailed = 'Variables call to the debugger failed';

var
  LocalsDlgWindowCreator: TIDEWindowCreator;

procedure CreateDebugDialog(Sender: TObject; aFormName: string; var AForm: TCustomForm; DoDisableAutoSizing: boolean);
begin
  AForm := TDebugExtensionVariablesDlg.Instance;
  AForm.Name := aFormName;
  AForm.DisableAutoSizing;
  if not DoDisableAutoSizing then
    AForm.EnableAutoSizing;
  IDEWindowCreators.ShowForm(AForm, True, vmOnlyMoveOffScreenToVisible);
end;

procedure ShowVariablesDialog(Sender: TObject);
var
  Frm: TCustomForm;
begin
  Frm := nil;
  CreateDebugDialog(Sender, 'ExtScopes', Frm, False);
end;

procedure Register;
begin
  RegisterIdeMenuCommand(itmViewDebugWindows, sVariables, sVariablesCaption, nil, @ShowVariablesDialog);

  LocalsDlgWindowCreator := IDEWindowCreators.Add('ExtScopes');
  LocalsDlgWindowCreator.OnCreateFormProc := @CreateDebugDialog;
  LocalsDlgWindowCreator.CreateSimpleLayout;
end;

{ TDebugExtensionVariablesDlg }

class function TDebugExtensionVariablesDlg.GetInstance: TDebugExtensionVariablesDlg; static;
begin
  if not Assigned(FInstance) then
    FInstance := TDebugExtensionVariablesDlg.Create(Application);
  Result := FInstance;
end;

procedure TDebugExtensionVariablesDlg.FormCreate(Sender: TObject);
begin
  IDEDialogLayoutList.ApplyLayout(Self,400,300);
  FController := TLazDebugExtensionStringTreeController.Create(vstVariables);
  TLazDebugExtensionDebugManager.AddEventListener(@HandleExtDebugEvent);

  RefreshScopes;
end;

procedure TDebugExtensionVariablesDlg.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  IDEDialogLayoutList.SaveLayout(Self);
end;

procedure TDebugExtensionVariablesDlg.HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent);
var
  State: TLazExtEvaluationState;
begin
  case AnExtDebugEvent of
    edeEvalutionStateChanged:
      begin
      State := TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState;
      if State = esEvaluationPossible then
        RefreshScopes;
      end;
  end;
end;

procedure TDebugExtensionVariablesDlg.RefreshScopes;
var
  Debugger: ILazDebugExtensionDebuggerIntf;
begin
  Debugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  if Assigned(Debugger) then
    begin
    if Debugger.GetEvaluationState<>esEvaluationPossible then
      Exit;

    FController.SetLoading;

    // FLatestRef is used to detect answers for outdated requests.
    Inc(FLatestRef);
    if not Debugger.RequestVariables(Debugger.GetCurrentFrameID, FLatestRef, @RequestCallback) then
      begin
      RequestCallback(FLatestRef, False, sRequestVariablesCallFailed, nil);
      end;
    end
  else
    RequestCallback(FLatestRef, False, sNoDebugger, nil);
end;

procedure TDebugExtensionVariablesDlg.RequestCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariableList: TLazDebugExtensionVariableList);
var
  UsedVariableList: TLazDebugExtensionVariableList;
begin
  if ARef<>FLatestRef then
    begin
    // This is a response to an old evaluation-call, ignore the result.
    AVariableList.Free;
    Exit;
    end;

  UsedVariableList := AVariableList;
  if not ASucceeded then
    begin
    if not Assigned(UsedVariableList) then
      begin
      UsedVariableList := TLazDebugExtensionVariableList.Create(True);
      UsedVariableList.Add(TLazDebugExtensionVariable.Create(Message, '', '', ARef));
      end
    end
  else if not Assigned(UsedVariableList) then
    begin
    UsedVariableList := TLazDebugExtensionVariableList.Create(True);
    UsedVariableList.Add(TLazDebugExtensionVariable.Create('No result', '', '', ARef));
    end;
  FController.BindVariableList(UsedVariableList);
end;

procedure TDebugExtensionVariablesDlg.FormDestroy(Sender: TObject);
begin
  TLazDebugExtensionDebugManager.RemoveEventListener(@HandleExtDebugEvent);
  FController.Free;
end;

end.

