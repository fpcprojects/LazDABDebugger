# Lazarus Debugging Extensions

This is a set of extensions for the Lazarus debugging capabilities. A Lazarus debug-backend can use these extensions by implementing the interface.
For now there is only one extension:

 * Tree-like view of variables (evaluation window)

## Getting started

Just install a Lazarus debug-backend that uses these extensions and you are ready to go.

### Prerequisites

To compile LazDABDebugger you need [Free Pascal](https://www.freepascal.org/) version 3.2 or above and [Lazarus](https://www.lazarus-ide.org/).

## How to add support for these extensions to a Lazarus debug-backend?

The debugger-backend has to:

* Implement the `ILazDebugExtensionDebuggerIntf` interface
* Call `TLazDebugExtensionDebugManager.MarkActiveDebugger` when it becomes the active debugger (could be done in a derivation of TDebuggerIntf.Init)
* Call `TLazDebugExtensionDebugManager.MarkDebuggerDisposed` as soon it is not the active debugger anymore (could be done in a derivation of TDebuggerIntf.Destroy)
* Send the proper events through `TLazDebugExtensionDebugManager.SendEvent`

## Versioning

This package is very new and receives a lot of updates. For the versions available, see the tags on this repository.

## Authors

* **Joost van der Sluis** - *initial work* - <joost@cnoc.nl>

## License

This library is distributed under the GNU General Public License version 2 (see the [COPYING.GPL.txt](COPYING.GPL.txt) file) with the following modification:

- object files and libraries linked into an application may be distributed without source code.