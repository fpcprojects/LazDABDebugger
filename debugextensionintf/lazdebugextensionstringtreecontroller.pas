{ Controller-class that binds a TLazVirtualStringTree to a list of variables

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDebugExtensionStringTreeController;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Graphics,
  ExtCtrls,
  Laz.VirtualTrees,
  Generics.Collections,
  LazDebugExtensionDebuggerIntf;

type
  TDebugRefNodeDictionary = specialize TDictionary<PtrInt, PVirtualNode>;

  { TLazDebugExtensionStringTreeController }

  TLazDebugExtensionStringTreeController = class
  private
    procedure NodeSetLoadingFlag(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
    procedure ChangePeriodEnd(Sender: TObject);
    procedure NodeClearChangedFlag(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
    procedure GracePeriodEnd(Sender: TObject);
  protected type
    TLazDebugExtensionVariableFlag = (vfLoading, vfChanged, vfOutdated);
    TLazDebugExtensionVariableFlags = set of TLazDebugExtensionVariableFlag;
  public type // BUG, FPC 3.3.1: protected types are not accessible by descendents
    TVariableNodeData = record
      Flags: TLazDebugExtensionVariableFlags;
      Variable: TLazDebugExtensionVariable;
    end;
  protected type
    PVariableNodeData = ^TVariableNodeData;

  protected
    FTree: TLazVirtualStringTree;

    // Keeps a list of references to nodes, the references are set in calls to
    // the debugger, and are used within the callbacks to retrieve the
    // corresponding nodes
    FRefNodeDict: TDebugRefNodeDictionary;
    FLatestRef: PtrInt;

    FTimedGracePeriod: Boolean;
    FGracePeriodTimer: TTimer;
    FChangedPeriodTimer: TTimer;

    procedure HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent); virtual;

    function GetEvaluationState: TLazExtEvaluationState;

    procedure BindVariableToNode(const ANode: PVirtualNode; AVariable: TLazDebugExtensionVariable);
    procedure LoadChilds(const ANode: PVirtualNode; const AVariable: TLazDebugExtensionVariable);
    procedure LoadChildsCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariableList: TLazDebugExtensionVariableList);
    procedure BindVariableListToNode(const AVariableList: TLazDebugExtensionVariableList; ANode: PVirtualNode);

    function NodeHasCaption(const ANode: PVirtualNode): Boolean; virtual;

    // Create a new reference for a specific node.
    function CreateRefForNode(ANode: PVirtualNode): PtrInt;
    // Retrieve the node that corresponds to a reference. Returns false in
    // case the reference is invalid. This is also the case when the reference
    // is not valid anymore after a call to ClearReferences
    function GetNodeByRef(ARef: PtrInt; out ANode: PVirtualNode): Boolean;
    procedure SetLoadingFlags(Node: PVirtualNode);
    procedure ClearChangedFlags;

    // Tree-events
    procedure DoGetNodeDataSize(Sender: TBaseVirtualTree; var NodeDataSize: Integer); virtual;
    procedure DoTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string); virtual;
    procedure DoInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates); virtual;
    procedure DoFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode); virtual;
    procedure DoInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode; var ChildCount: Cardinal); virtual;
    procedure DoGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: string); virtual;
    procedure DoPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType); virtual;
    procedure DoBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect); virtual;
  public
    constructor Create(ATree: TLazVirtualStringTree);
    destructor Destroy; override;
    // Bind a list of variables to the TreeView. The controller will take ownership
    // of the list with all it's variables.
    procedure BindVariableList(AVariableList: TLazDebugExtensionVariableList);

    // Clears all variables. Note that this might be necessary before binding to
    // a new variable-list, to avoid changes being marked as changes (red)
    procedure ClearVariableList();

    // Mark all items as loading
    procedure SetLoading;
  end;

implementation

resourcestring
  sNoDebugger = 'No debugger available';
  sLoadVariablesCallFailed = 'Failed to retrieve the value of the variable from the debugger';

{ TLazDebugExtensionStringTreeController }

constructor TLazDebugExtensionStringTreeController.Create(ATree: TLazVirtualStringTree);
begin
  FTree := ATree;
  FTree.OnGetText := @DoTreeGetText;
  FTree.OnGetNodeDataSize := @DoGetNodeDataSize;
  FTree.OnInitNode := @DoInitNode;
  FTree.OnFreeNode := @DoFreeNode;
  FTree.OnInitChildren := @DoInitChildren;
  FTree.OnGetHint := @DoGetHint;
  FTree.OnPaintText := @DoPaintText;
  FTree.OnBeforeCellPaint := @DoBeforeCellPaint;

  FRefNodeDict := TDebugRefNodeDictionary.Create;

  FTree.TreeOptions.StringOptions := FTree.TreeOptions.StringOptions + [toShowStaticText];
  FTree.TreeOptions.MiscOptions := FTree.TreeOptions.MiscOptions - [toEditOnClick, toAcceptOLEDrop];
  FTree.TreeOptions.PaintOptions := FTree.TreeOptions.PaintOptions - [toShowTreeLines];
  FTree.Indent := 11;
  FTree.ButtonStyle := bsTriangle;
  FTree.HintMode := hmHint;
  FTree.ShowHint := True;


  FChangedPeriodTimer := TTimer.Create(FTree);
  FChangedPeriodTimer.Interval := 5000;
  FChangedPeriodTimer.OnTimer := @ChangePeriodEnd;

  FGracePeriodTimer := TTimer.Create(FTree);
  FGracePeriodTimer.Interval := 250;
  FGracePeriodTimer.OnTimer  := @GracePeriodEnd;

  TLazDebugExtensionDebugManager.AddEventListener(@HandleExtDebugEvent);
end;

procedure TLazDebugExtensionStringTreeController.BindVariableList(AVariableList: TLazDebugExtensionVariableList);
var
  Node: PVirtualNode;
  Variable: TLazDebugExtensionVariable;
begin
  FTree.RootNodeCount := AVariableList.Count;
  Node := FTree.GetFirst;
  BindVariableListToNode(AVariableList, Node);
  AVariableList.Free;
  FTree.Refresh;
end;

procedure TLazDebugExtensionStringTreeController.BindVariableToNode(const ANode: PVirtualNode; AVariable: TLazDebugExtensionVariable);
var
  OldVariable: TLazDebugExtensionVariable;
  ActiveDebugger: ILazDebugExtensionDebuggerIntf;
begin
  if Assigned(TVariableNodeData(FTree.GetNodeData(ANode)^).Variable) then
    OldVariable := TVariableNodeData(FTree.GetNodeData(ANode)^).Variable
  else
    OldVariable := nil;

  TVariableNodeData(FTree.GetNodeData(ANode)^).Variable := AVariable;

  if Assigned(OldVariable) and Assigned(AVariable) and
     ((OldVariable.Value<>AVariable.Value) or (OldVariable.ValueType<>AVariable.ValueType)) then
    begin
    // Reset the interval in case the timer was already enabled.
    FChangedPeriodTimer.Enabled := False;
    FChangedPeriodTimer.Enabled := True;
    TVariableNodeData(FTree.GetNodeData(ANode)^).Flags := [vfChanged]
    end
  else
    TVariableNodeData(FTree.GetNodeData(ANode)^).Flags := [];

  ActiveDebugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  // Also in case of an error, a (fake) variable with the error message is bound
  // to a node. In this case there might be no ActiveDebugger.
  if Assigned(ActiveDebugger) and ActiveDebugger.HasChildren(AVariable) then
    begin
    Include(ANode^.States, vsHasChildren);
    if (vsExpanded in ANode^.States) then
      begin
      // ToDo: Check same type (but how, this information is gone...)
      LoadChilds(FTree.GetFirstChild(ANode), AVariable);
      end;
    end
  else
    Exclude(ANode^.States, vsHasChildren);

  if Assigned(OldVariable) then
    OldVariable.Free;
end;

procedure TLazDebugExtensionStringTreeController.DoTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  Variable: TLazDebugExtensionVariable;
  EvalState: TLazExtEvaluationState;
  NodeData: PVariableNodeData;
begin
  EvalState := GetEvaluationState;
  if EvalState in [esNone, esEvaluationImpossible] then
    begin
    CellText := 'Invalid state';
    Exit;
    end;
  NodeData := PVariableNodeData(FTree.GetNodeData(Node));
  if not Assigned(NodeData) then
    begin
    CellText := 'Unknown node';
    Exit;
    end;

  Variable := NodeData^.Variable;
  if TextType = ttNormal then
    begin
    // Left part of text
    // When there is some extra information for the variable, this text contains
    // this extra information, like a caption and the actual value is in the
    // ttStaticText.
    // When there is no extra information (caption) this text contains the debug-
    // value.
    if not Assigned(Variable) or
       ((vfLoading in TVariableNodeData(FTree.GetNodeData(Node)^).Flags) and not (FTimedGracePeriod)) then
      begin
      if TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState=esEvaluationPossible then
        CellText := 'Loading...'
      else
        CellText := '';
      end
    else
      begin
      if NodeHasCaption(Node) then
        CellText := Variable.Name + ':'
      else
        begin
        if (TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState=esEvaluationPossible) or (FTimedGracePeriod) then
          CellText := Variable.Value
        else
          CellText := 'Unavailable';
        end;
      end
    end
  else
    begin
    // Rigth part of text
    // Contains the debug-value when there is some extra information (caption)
    CellText := '';
    if (TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState=esEvaluationPossible) or (FTimedGracePeriod) then
      begin
      if Assigned(Variable) and NodeHasCaption(Node) then
        CellText := Variable.Value
      end
    else
      begin
      if NodeHasCaption(Node) then
        CellText := 'Unavailable'
      else
        CellText := '';
      end;
    end;
end;

destructor TLazDebugExtensionStringTreeController.Destroy;
begin
  TLazDebugExtensionDebugManager.RemoveEventListener(@HandleExtDebugEvent);

  // Clear all nodes, before OnFreeNode is unassigned, to avoid memory leaks.
  FTree.Clear;

  FreeAndNil(FRefNodeDict);

  FTree.OnGetText := nil;
  FTree.OnGetNodeDataSize := nil;
  FTree.OnInitNode := nil;
  FTree.OnFreeNode := nil;
  FTree.OnInitChildren := nil;
  FTree.OnGetHint := nil;
  FTree.OnPaintText := nil;
  FTree.OnBeforeCellPaint := nil;
  inherited Destroy;
end;

procedure TLazDebugExtensionStringTreeController.DoGetNodeDataSize(Sender: TBaseVirtualTree; var NodeDataSize: Integer);
begin
  NodeDataSize := SizeOf(TVariableNodeData);
end;

procedure TLazDebugExtensionStringTreeController.DoInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
  with TVariableNodeData(Sender.GetNodeData(Node)^) do
    begin
    Variable := nil;
    Flags := [];
    end;
end;

procedure TLazDebugExtensionStringTreeController.DoFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  TVariableNodeData(Sender.GetNodeData(Node)^).Variable.Free;
end;

procedure TLazDebugExtensionStringTreeController.DoInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode; var ChildCount: Cardinal);
var
  ChildNode: PVirtualNode;
begin
  if toReadOnly in FTree.TreeOptions.MiscOptions then
    // We're read only. Probably because the debugee is running. It is not possible
    // to explore children at this moment.
    Exit;
  ChildNode := FTree.AddChild(Node);
  if not Assigned(ChildNode) then
    raise Exception.Create('Failed to add childnode');
  LoadChilds(ChildNode, TVariableNodeData(FTree.GetNodeData(Node)^).Variable);
  ChildCount := 1;
end;

procedure TLazDebugExtensionStringTreeController.LoadChilds(const ANode: PVirtualNode; const AVariable: TLazDebugExtensionVariable);
var
  Debugger: ILazDebugExtensionDebuggerIntf;
begin
  Debugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  if Assigned(Debugger) then
    begin
    if Assigned(AVariable) and Debugger.HasChildren(AVariable) then
      begin
      if not Debugger.LoadChilds(AVariable, CreateRefForNode(ANode), @LoadChildsCallback) then
        begin
        LoadChildsCallback(FLatestRef, False, sLoadVariablesCallFailed, nil);
        end;
      end
    else
      // Remove the temporary child node
      FTree.DeleteNode(ANode);
    end
  else
    LoadChildsCallback(CreateRefForNode(ANode), False, sNoDebugger, nil);
end;

function TLazDebugExtensionStringTreeController.CreateRefForNode(ANode: PVirtualNode): PtrInt;
begin
  if Assigned(FRefNodeDict) then
    begin
    Inc(FLatestRef);
    Result := FLatestRef;
    FRefNodeDict.Add(Result, ANode);
    end
  else
    Result := 0;
end;

function TLazDebugExtensionStringTreeController.GetNodeByRef(ARef: PtrInt; out ANode: PVirtualNode): Boolean;
begin
  Result := FRefNodeDict.TryGetValue(ARef, ANode);
end;

procedure TLazDebugExtensionStringTreeController.LoadChildsCallback(
  const ARef: PtrInt;
  const ASucceeded: Boolean;
  const Message: string;
  const AVariableList: TLazDebugExtensionVariableList);
var
  Node: PVirtualNode;
  Variable: TLazDebugExtensionVariable;
begin
  if GetNodeByRef(ARef, Node) then
    begin
    if not ASucceeded then
      begin
      Variable := TLazDebugExtensionVariable.Create(Message, '', '', 0);
      BindVariableToNode(Node, Variable);
      end
    else if not Assigned(AVariableList) then
      begin
      Variable := TLazDebugExtensionVariable.Create('No result', '', '', 0);
      BindVariableToNode(Node, Variable);
      end
    else
      begin
      FTree.ChildCount[Node^.Parent] := AVariableList.Count;
      BindVariableListToNode(AVariableList, Node);
      end;
    AVariableList.Free;
    FTree.Refresh;
    end
end;

procedure TLazDebugExtensionStringTreeController.BindVariableListToNode(const AVariableList: TLazDebugExtensionVariableList; ANode: PVirtualNode);
var
  Variable: TLazDebugExtensionVariable;
begin
  if AVariableList.Count > 0 then
    begin
    while AVariableList.Count > 0 do
      begin
      Variable := AVariableList.ExtractIndex(0);
      BindVariableToNode(ANode, Variable);
      ANode := FTree.GetNextSiblingNoInit(ANode);
      end;
    end;
end;

function TLazDebugExtensionStringTreeController.GetEvaluationState: TLazExtEvaluationState;
var
  ActiveDebugger: ILazDebugExtensionDebuggerIntf;
begin
  ActiveDebugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  if Assigned(ActiveDebugger) then
    Result := ActiveDebugger.GetEvaluationState
  else
    Result := esNone;
end;

procedure TLazDebugExtensionStringTreeController.DoGetHint(
  Sender: TBaseVirtualTree;
  Node: PVirtualNode;
  Column: TColumnIndex;
  var LineBreakStyle: TVTTooltipLineBreakStyle;
  var HintText: string);
var
  Variable: TLazDebugExtensionVariable;
begin
  Variable := TVariableNodeData(FTree.GetNodeData(Node)^).Variable;
  if Assigned(Variable) then
    begin
    HintText := Variable.ValueType;
    end
end;

procedure TLazDebugExtensionStringTreeController.DoPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType);
var
  NodeData: TVariableNodeData;
begin
  // The actual debug-value is highlighted (clMaroon), as it is the most
  // important part.
  NodeData := TVariableNodeData(FTree.GetNodeData(Node)^);
  if not Assigned(NodeData.Variable) or ((vfLoading in NodeData.Flags) and not (FTimedGracePeriod)) then
    TargetCanvas.Font.Color := clGrayText
  else if (TextType = ttNormal) and NodeHasCaption(Node) then
    // There is a 'caption', so the normal text contains this caption and
    // should not be highlighted.
    TargetCanvas.Font.Color := clGrayText
  else
    TargetCanvas.Font.Color := clMaroon
end;

procedure TLazDebugExtensionStringTreeController.DoBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode;
  CellRect: TRect; var ContentRect: TRect);
var
  NodeData: TVariableNodeData;
  BackgroundRect: TRect;
  StaticText: string;
  NormalText: string;

begin
  if CellPaintMode=cpmPaint then
    begin
    NodeData := TVariableNodeData(FTree.GetNodeData(Node)^);
    if vfChanged in NodeData.Flags then
      begin
      TargetCanvas.Brush.Color := clRed;
      TargetCanvas.Brush.Style := bsSolid;
      BackgroundRect.TopLeft := ContentRect.TopLeft;

      StaticText := FTree.StaticText[Node, Column];
      NormalText := FTree.Text[Node, Column];

      if StaticText='' then
        begin
        // There is no statictext, which means that the normal text contains the
        // debug-value, and that is the one that has to be highlighted.
        BackgroundRect.Width := TargetCanvas.Font.GetTextWidth(NormalText) + (TVirtualStringTree(Sender).TextMargin * 2);
        end
      else
        begin
        // There is a statictext, which has to contain the debug-value, so highlight
        // the static-text.

        // Would be better to use NodeWith, but that one is protected.
        BackgroundRect.Offset(TVirtualStringTree(Sender).TextMargin*2 + TargetCanvas.Font.GetTextWidth(NormalText), 0);
        BackgroundRect.Width := TargetCanvas.Font.GetTextWidth(StaticText) + (TVirtualStringTree(Sender).TextMargin * 2);
        end;
      BackgroundRect.Height := TargetCanvas.Font.GetTextHeight(NodeData.Variable.Value);
      TargetCanvas.Rectangle(BackgroundRect);
      end;
    end;
end;

procedure TLazDebugExtensionStringTreeController.HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent);
var
  State: TLazExtEvaluationState;
begin
  case AnExtDebugEvent of
    edeEvalutionStateChanged:
      begin
      State := TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState;
      case State of
        esEvaluationPossible:
          begin
          FTree.TreeOptions.MiscOptions := FTree.TreeOptions.MiscOptions - [toReadOnly];
          end;
        esEvaluationImpossible:
          begin
          FTree.TreeOptions.MiscOptions := FTree.TreeOptions.MiscOptions - [toReadOnly];
          ClearChangedFlags;
          //ClearReferences;
          FTree.Clear;
          end;
        esEvaluationPaused:
          begin
          ClearChangedFlags;
          //ClearReferences;
          FTimedGracePeriod := True;
          FGracePeriodTimer.Enabled := True;
          FTree.TreeOptions.MiscOptions := FTree.TreeOptions.MiscOptions + [toReadOnly];
          end;
      end;
      end;
  end;
  FTree.Refresh;
end;

procedure TLazDebugExtensionStringTreeController.SetLoading;
begin
  if FTree.RootNodeCount=0 then
    FTree.RootNodeCount := 1;

  // Enable the loading flag for all items. Enable the grace period, so that
  // the texts of the items are not changed to 'loading' immediately, but only
  // after the grace-period.
  SetLoadingFlags(nil);
  FTimedGracePeriod := True;
  FGracePeriodTimer.Enabled := True;
end;

procedure TLazDebugExtensionStringTreeController.SetLoadingFlags(Node: PVirtualNode);
begin
  if FTree.RootNodeCount>0 then
    FTree.IterateSubtree(Node, @NodeSetLoadingFlag, nil);
end;

procedure TLazDebugExtensionStringTreeController.NodeSetLoadingFlag(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
begin
  TVariableNodeData(Sender.GetNodeData(Node)^).Flags := TVariableNodeData(Sender.GetNodeData(Node)^).Flags + [vfLoading];
end;

procedure TLazDebugExtensionStringTreeController.ChangePeriodEnd(Sender: TObject);
begin
  FChangedPeriodTimer.Enabled := False;
  ClearChangedFlags;
end;

procedure TLazDebugExtensionStringTreeController.ClearChangedFlags;
begin
  if FTree.RootNodeCount>0 then
    FTree.IterateSubtree(nil, @NodeClearChangedFlag, nil);
  FTree.Refresh;
end;

procedure TLazDebugExtensionStringTreeController.NodeClearChangedFlag(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
begin
  Exclude(TVariableNodeData(FTree.GetNodeData(Node)^).Flags, vfChanged);
end;

procedure TLazDebugExtensionStringTreeController.GracePeriodEnd(Sender: TObject);
begin
  FGracePeriodTimer.Enabled := False;
  FTimedGracePeriod := False;
  FTree.Refresh;
end;

procedure TLazDebugExtensionStringTreeController.ClearVariableList();
begin
  BindVariableList(TLazDebugExtensionVariableList.Create);
end;

function TLazDebugExtensionStringTreeController.NodeHasCaption(const ANode: PVirtualNode): Boolean;
var
  NodeData: PVariableNodeData;
  Variable: TLazDebugExtensionVariable;
begin
  NodeData := PVariableNodeData(FTree.GetNodeData(ANode));
  Variable := NodeData^.Variable;
  result := Assigned(Variable) and (Variable.Name<>'');
end;

end.

