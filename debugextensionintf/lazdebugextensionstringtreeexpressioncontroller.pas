unit LazDebugExtensionStringTreeExpressionController;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Laz.VirtualTrees,
  LazDebugExtensionDebuggerIntf,
  LazDebugExtensionStringTreeController;

type

  { TLazDebugExtensionStringTreeExpressionController }

  TLazDebugExtensionStringTreeExpressionController = class(TLazDebugExtensionStringTreeController)
  private
    procedure NodeSetOutdatedFlag(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
  protected type
    TVariableOrExpressionNodeData = record
      VariableData: TVariableNodeData;
      Expression: string;
    end;
    PVariableOrExpressionNodeData = ^TVariableOrExpressionNodeData;
  protected
    procedure MarkAllNodesOutdated();

    procedure BindExpressionToNode(const ANode: PVirtualNode; AnExpression: string);
    procedure AssignExpressionListToNode(const AnExpressionList: TStrings; ANode: PVirtualNode);
    procedure EvaluateExpression(const ANode: PVirtualNode);
    procedure EvaluateCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariable: TLazDebugExtensionVariable);

    function NodeHasCaption(const ANode: PVirtualNode): Boolean; override;
    procedure HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent); override;

    // Tree-events
    procedure DoGetNodeDataSize(Sender: TBaseVirtualTree; var NodeDataSize: Integer); override;
    procedure DoInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates); override;
    procedure DoFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode); override;
    procedure DoTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string); override;
  public
    // Assign a list of expressions to the TreeView.
    procedure AssignExpressionList(AnExpressionList: TStrings);
    function GetExpressionBoundToNode(ANode: PVirtualNode): string;

  end;


implementation

resourcestring
  sNoDebugger = 'No debugger available';
  sEvaluateExpressionCallFailed = 'Failed to evaluate the expression from the debugger';

{ TLazDebugExtensionStringTreeExpressionController }

procedure TLazDebugExtensionStringTreeExpressionController.AssignExpressionList(AnExpressionList: TStrings);
var
  Node: PVirtualNode;
  RO: Boolean;
begin
  RO := toReadOnly in FTree.TreeOptions.MiscOptions;
  if RO then
    FTree.TreeOptions.MiscOptions := FTree.TreeOptions.MiscOptions - [toReadOnly];

  FTree.RootNodeCount := AnExpressionList.Count;
  Node := FTree.GetFirst;
  AssignExpressionListToNode(AnExpressionList, Node);

  if RO then
    FTree.TreeOptions.MiscOptions := FTree.TreeOptions.MiscOptions + [toReadOnly];

  FTree.Refresh;
end;

procedure TLazDebugExtensionStringTreeExpressionController.AssignExpressionListToNode(const AnExpressionList: TStrings; ANode: PVirtualNode);
var
  Expression: string;
  i: Integer;
begin
  if AnExpressionList.Count > 0 then
    begin
    for i := 0 to AnExpressionList.Count -1 do
      begin
      Expression := AnExpressionList[i];
      BindExpressionToNode(ANode, Expression);
      ANode := FTree.GetNextSiblingNoInit(ANode);
      end;
    end;
end;

procedure TLazDebugExtensionStringTreeExpressionController.BindExpressionToNode(const ANode: PVirtualNode; AnExpression: string);
var
  NodeData: PVariableOrExpressionNodeData;
begin
  NodeData := PVariableOrExpressionNodeData(FTree.GetNodeData(ANode));
  if NodeData^.Expression <> AnExpression then
    begin
    NodeData^.Expression := AnExpression;

    if GetEvaluationState = esEvaluationPossible then
      // Setting this flags makes sure that the watch is evaluated when it's
      // value is required by the GUI.
      NodeData^.VariableData.Flags := [vfOutdated];

    Exclude(ANode^.States, vsHasChildren);
    end;
end;

procedure TLazDebugExtensionStringTreeExpressionController.DoFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  inherited DoFreeNode(Sender, Node);
  TVariableOrExpressionNodeData(Sender.GetNodeData(Node)^).Expression := '';
end;

procedure TLazDebugExtensionStringTreeExpressionController.DoGetNodeDataSize(Sender: TBaseVirtualTree; var NodeDataSize: Integer);
begin
  NodeDataSize := SizeOf(TVariableOrExpressionNodeData);
end;

procedure TLazDebugExtensionStringTreeExpressionController.DoInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
  inherited DoInitNode(Sender, ParentNode, Node, InitialStates);
  with TVariableOrExpressionNodeData(Sender.GetNodeData(Node)^) do
    begin
    Expression := '';
    end;
end;

procedure TLazDebugExtensionStringTreeExpressionController.DoTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  EvalState: TLazExtEvaluationState;
  NodeData: PVariableOrExpressionNodeData;
begin
  EvalState := GetEvaluationState;
  NodeData := PVariableOrExpressionNodeData(Sender.GetNodeData(Node));
  if not (EvalState in [esNone, esEvaluationImpossible]) and
     Assigned(NodeData) then
    begin
    if (vfOutdated in TVariableOrExpressionNodeData(NodeData^).VariableData.Flags) and
       not (vfLoading in TVariableOrExpressionNodeData(NodeData^).VariableData.Flags) then
      begin
      // Find the parent-node with the expression.
      while Assigned(NodeData) and (NodeData^.Expression='') do
        begin
        Node := Node^.Parent;
        if Assigned(Node) then
          NodeData := PVariableOrExpressionNodeData(Sender.GetNodeData(Node))
        else
          NodeData := nil;
        end;
      if Assigned(Node) then
        EvaluateExpression(Node);
      end;

    if (NodeData^.Expression <> '') then
      begin
      if (TextType = ttNormal) then
        CellText := TVariableOrExpressionNodeData(Sender.GetNodeData(Node)^).Expression + ':'
      else
        inherited DoTreeGetText(Sender, Node, Column, TextType, CellText);
      end
    else
      inherited DoTreeGetText(Sender, Node, Column, TextType, CellText);
    end
  else
    inherited DoTreeGetText(Sender, Node, Column, TextType, CellText);
end;

procedure TLazDebugExtensionStringTreeExpressionController.EvaluateExpression(const ANode: PVirtualNode);
var
  Debugger: ILazDebugExtensionDebuggerIntf;
  Expression: string;
begin
  SetLoadingFlags(ANode);

  Debugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  if Assigned(Debugger) then
    begin
    Expression := TVariableOrExpressionNodeData(FTree.GetNodeData(ANode)^).Expression;
    if not Debugger.EvaluateExpression(Expression, Debugger.GetCurrentFrameID, CreateRefForNode(ANode), @EvaluateCallback) then
      EvaluateCallback(CreateRefForNode(ANode), False, sEvaluateExpressionCallFailed, nil);
    end
  else
    EvaluateCallback(CreateRefForNode(ANode), False, sNoDebugger, nil);
end;

procedure TLazDebugExtensionStringTreeExpressionController.EvaluateCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariable: TLazDebugExtensionVariable);
var
  Node: PVirtualNode;
  Variable: TLazDebugExtensionVariable;
  NodeData: PVariableOrExpressionNodeData;
begin
  if GetNodeByRef(ARef, Node) then
    begin
    NodeData := PVariableOrExpressionNodeData(FTree.GetNodeData(Node));

    if not ASucceeded then
      begin
      Variable := TLazDebugExtensionVariable.Create(Message, NodeData^.Expression, '', 0);
      BindVariableToNode(Node, Variable);
      end
    else if not Assigned(AVariable) then
      begin
      Variable := TLazDebugExtensionVariable.Create('No result', NodeData^.Expression, '', 0);
      BindVariableToNode(Node, Variable);
      end
    else
      begin
      BindVariableToNode(Node, AVariable);
      end;
    FTree.Refresh;
    end
end;

function TLazDebugExtensionStringTreeExpressionController.NodeHasCaption(const ANode: PVirtualNode): Boolean;
begin
  if PVariableOrExpressionNodeData(FTree.GetNodeData(ANode))^.Expression<>'' then
    Result := True
  else
    Result := inherited NodeHasCaption(ANode);
end;

procedure TLazDebugExtensionStringTreeExpressionController.HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent);
var
  State: TLazExtEvaluationState;
begin
  case AnExtDebugEvent of
    edeEvalutionStateChanged:
      begin
      State := TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState;
      case State of
        esEvaluationPossible:
          begin
          MarkAllNodesOutdated;
          end;
        esEvaluationImpossible:
          begin
          ClearChangedFlags;
          // Do not clear the tree, but keep showing the expressions
          Exit;
          end;
      end; {case}
      end;
  end;
  inherited HandleExtDebugEvent(AnExtDebugEvent);
end;

procedure TLazDebugExtensionStringTreeExpressionController.MarkAllNodesOutdated();
begin
  if FTree.RootNodeCount>0 then
    FTree.IterateSubtree(nil, @NodeSetOutdatedFlag, nil);
  FTree.Refresh;
end;

procedure TLazDebugExtensionStringTreeExpressionController.NodeSetOutdatedFlag(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer; var Abort: Boolean);
begin
  Include(TVariableNodeData(FTree.GetNodeData(Node)^).Flags, vfOutdated);
end;

function TLazDebugExtensionStringTreeExpressionController.GetExpressionBoundToNode(ANode: PVirtualNode): string;
begin
  Result := PVariableOrExpressionNodeData(FTree.GetNodeData(ANode))^.Expression;
end;

end.

