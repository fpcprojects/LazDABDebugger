{ Additional Evaluation-dialog, which shows the data in a tree-like manner.

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDebugExtensionEvaluateDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ExtCtrls,
  StdCtrls,
  ComCtrls,
  MenuIntf,
  LCLType,
  IDEWindowIntf,
  laz.VirtualTrees,
  LazDebugExtensionDebuggerIntf,
  LazDebugExtensionStringTreeController;

type

  TDebugRefNodeDictionary = specialize TDictionary<PtrInt, PVirtualNode>;

  { TDebugExtensionEvaluateDlg }

  TDebugExtensionEvaluateDlg = class(TForm)
    Label1: TLabel;
    cmbExpression: TComboBox;
    Label2: TLabel;
    ToolBar1: TToolBar;
    tbInspect: TToolButton;
    tbWatch: TToolButton;
    tbEvaluate: TToolButton;
    vstResult: TLazVirtualStringTree;
    procedure tbEvaluateClick(Sender: TObject);
    procedure cmbExpressionChange(Sender: TObject);
    procedure cmbExpressionKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cmbExpressionKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cmbExpressionSelect(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  protected
    FController: TLazDebugExtensionStringTreeController;

    FSkipKeySelect: Boolean;
    FLatestRef: Integer;

    // Is set when the expression has been changed since the last call to
    // evaluate;
    FExpressionChanged: boolean;
    procedure Evaluate;
    procedure EvaluateCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariable: TLazDebugExtensionVariable);
    function GetEvaluationState: TLazExtEvaluationState;
    procedure HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent);
    procedure UpdateButtonStates();
  private
    class var FInstance: TDebugExtensionEvaluateDlg;
    class function GetInstance: TDebugExtensionEvaluateDlg; static;
  public
    class property Instance: TDebugExtensionEvaluateDlg read GetInstance;
  end;

procedure Register;

implementation

{$R *.lfm}

resourcestring
  sEvaluate = 'Evaluate';
  sEvaluateCaption = 'Tree-based evaluation window';
  sNoDebugger = 'No debugger available';
  sEvaluateCallFailed = 'Evaluation call to the debugger failed';

procedure ShowEvaluateDialog(Sender: TObject);
begin
  TDebugExtensionEvaluateDlg.Instance.Show;
  TDebugExtensionEvaluateDlg.Instance.BringToFront;
end;

procedure Register;
begin
  RegisterIdeMenuCommand(itmViewDebugWindows, sEvaluate, sEvaluateCaption, nil, @ShowEvaluateDialog);
end;

{ TDebugExtensionEvaluateDlg }

procedure TDebugExtensionEvaluateDlg.tbEvaluateClick(Sender: TObject);
begin
  Evaluate;
end;

procedure TDebugExtensionEvaluateDlg.Evaluate;
var
  S: String;
  Debugger: ILazDebugExtensionDebuggerIntf;
begin
  S := cmbExpression.Text;

  if S = '' then Exit;
  Debugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  if Assigned(Debugger) then
    begin
    if Debugger.GetEvaluationState<>esEvaluationPossible then
      Exit;

    // FLatestRef is used to detect answers for outdated requests.
    Inc(FLatestRef);

    if FExpressionChanged then
      FController.ClearVariableList;
    FController.SetLoading;

    FExpressionChanged := False;
    if not Debugger.EvaluateExpression(S, Debugger.GetCurrentFrameID, FLatestRef, @EvaluateCallback) then
      EvaluateCallback(FLatestRef, False, sEvaluateCallFailed, nil);
    end
  else
    EvaluateCallback(FLatestRef, False, sNoDebugger, nil);
end;

class function TDebugExtensionEvaluateDlg.GetInstance: TDebugExtensionEvaluateDlg; static;
begin
  if not Assigned(FInstance) then
    FInstance := TDebugExtensionEvaluateDlg.Create(Application);
  Result := FInstance;
end;

procedure TDebugExtensionEvaluateDlg.cmbExpressionChange(Sender: TObject);
begin
  FExpressionChanged := True;
  UpdateButtonStates;
end;

procedure TDebugExtensionEvaluateDlg.UpdateButtonStates;
var
  HasExpression: Boolean;
begin
  HasExpression := Trim(cmbExpression.Text) <> '';
  tbEvaluate.Enabled := HasExpression and FExpressionChanged and (GetEvaluationState=esEvaluationPossible);
  tbWatch.Enabled := HasExpression;
  tbInspect.Enabled := HasExpression and (GetEvaluationState=esEvaluationPossible);
end;

procedure TDebugExtensionEvaluateDlg.cmbExpressionKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  FSkipKeySelect := True;
  if (Key = VK_RETURN) and tbEvaluate.Enabled
  then begin
    Evaluate;
    Key := 0;
  end;
end;

procedure TDebugExtensionEvaluateDlg.cmbExpressionKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  FSkipKeySelect := False;
end;

procedure TDebugExtensionEvaluateDlg.cmbExpressionSelect(Sender: TObject);
begin
  if not FSkipKeySelect then
    Evaluate;
end;

procedure TDebugExtensionEvaluateDlg.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  IDEDialogLayoutList.SaveLayout(Self);
end;

procedure TDebugExtensionEvaluateDlg.FormCreate(Sender: TObject);
begin
  IDEDialogLayoutList.ApplyLayout(Self,400,300);
  FController := TLazDebugExtensionStringTreeController.Create(vstResult);
  TLazDebugExtensionDebugManager.AddEventListener(@HandleExtDebugEvent);
end;

procedure TDebugExtensionEvaluateDlg.EvaluateCallback(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariable: TLazDebugExtensionVariable);
var
  UsedVariableList: TLazDebugExtensionVariableList;
begin
  if ARef<>FLatestRef then
    begin
    // This is a response to an old evaluation-call, ignore the result.
    AVariable.Free;
    Exit;
    end;

  UsedVariableList := TLazDebugExtensionVariableList.Create(True);

  if Assigned(AVariable) then
    begin
    UsedVariableList.Add(AVariable);
    end
  else
    begin
    if not ASucceeded then
      UsedVariableList.Add(TLazDebugExtensionVariable.Create(Message, '', '', ARef))
    else
      UsedVariableList.Add(TLazDebugExtensionVariable.Create('No result', '', '', ARef));
    end;

  FController.BindVariableList(UsedVariableList);
end;

procedure TDebugExtensionEvaluateDlg.FormDestroy(Sender: TObject);
begin
  TLazDebugExtensionDebugManager.RemoveEventListener(@HandleExtDebugEvent);
  FController.Free;
end;

procedure TDebugExtensionEvaluateDlg.HandleExtDebugEvent(AnExtDebugEvent: TLazExtDebugEvent);
var
  State: TLazExtEvaluationState;
begin
  case AnExtDebugEvent of
    edeEvalutionStateChanged:
      begin
      State := TLazDebugExtensionDebugManager.ActiveDebugger.GetEvaluationState;
      case State of
        esEvaluationPossible:
          begin
          Evaluate;
          end;
      end;
      end;
  end;
  UpdateButtonStates;
end;

function TDebugExtensionEvaluateDlg.GetEvaluationState: TLazExtEvaluationState;
var
  ActiveDebugger: ILazDebugExtensionDebuggerIntf;
begin
  ActiveDebugger := TLazDebugExtensionDebugManager.ActiveDebugger;
  if Assigned(ActiveDebugger) then
    Result := ActiveDebugger.GetEvaluationState
  else
    Result := esNone;
end;

end.

