{ Basic interfase for the Lazarus Debug extensions

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}

unit LazDebugExtensionDebuggerIntf;

{$mode objfpc}{$H+}
{$INTERFACES CORBA} // no ref counting needed

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  MenuIntf;

type

  { TLazDebugExtensionVariable }

  // This class is used to show debug-information in the GUI.
  TLazDebugExtensionVariable = class
  private
    FValue: string;
    FName: string;
    FValueType: string;
    FReference: PtrInt;
  public
    constructor Create(const AValue, AName, AValueType: string; AReference: PtrInt);
    // Name of the variable, when the variable is a property, for example, this
    // contains the property name.
    property Name: string read FName;
    // A string representation of the actual value of the variable.
    property Value: string read FValue;
    // The type of the variable
    property ValueType: string read FValueType;
    // A reference that could be used to gather more information about this
    // variable. (for example a list of childs)
    property Reference: PtrInt read FReference;
  end;
  TLazDebugExtensionVariableList = specialize TObjectList<TLazDebugExtensionVariable>;

  // The TLazDebugExtensionDebugManager emits events when the GUI might need to
  // respond to certain debug-events. This is a list with the possible reasons
  // why such an event is triggered.
  TLazExtDebugEvent = (
    edeEvalutionStateChanged // The EvaluationState has been changed.
  );

  // The GUI does not need to know what the exact debug state is to be able to
  // show debug-information. This is what the GUI actually needs. (I hope)
  TLazExtEvaluationState = (
    esNone,                  // No debugger available
    esEvaluationImpossible,  // Evaluation of variables is impossible (Maybe because there is nu debugee? (stopped))
    esEvaluationPaused,      // Evaluation of variables is temporarily disabled (Maybe because debugee is running?)
    esEvaluationPossible     // Evaluation of variables is possible (Debugee is probably paused)
  );

  TLazDebugExtensionEvaluateCallback = procedure(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariable:TLazDebugExtensionVariable) of object;
  TLazDebugExtensionVariablesCallback = procedure(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariableList:TLazDebugExtensionVariableList) of object;
  TLazDebugExtensionLoadChildsCallback = procedure(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AVariableList: TLazDebugExtensionVariableList) of object;
  TLazDebugEventListenerProc = procedure(AnExtDebugEvent: TLazExtDebugEvent) of object;
  TLazDebugEventListenerList = specialize TList<TLazDebugEventListenerProc>;

  { ILazDebugExtensionDebuggerIntf }

  // A Debugger (TDebuggerIntf) has to implement this interface to be able to
  // use the Lazarus debug-extensions.
  ILazDebugExtensionDebuggerIntf = interface ['{EA52303B-6B8C-4963-A4FC-0EE7DFCE3723}']
    function GetCurrentFrameID: Integer;
    function EvaluateExpression(const AnExpression: string; const AFrameId: Integer; const ARef: PtrInt; const ACallBack: TLazDebugExtensionEvaluateCallback): Boolean;
    function RequestVariables(const AFrameId: Integer; const ARef: PtrInt; const ACallBack: TLazDebugExtensionVariablesCallback): Boolean;
    function LoadChilds(const AVariable: TLazDebugExtensionVariable; const ARef: PtrInt; const ACallBack: TLazDebugExtensionLoadChildsCallback): Boolean;
    function HasChildren(const AVariable: TLazDebugExtensionVariable): Boolean;
    function GetEvaluationState: TLazExtEvaluationState;
  end;

  { TLazDebugExtensionDebugManager }

  // This class manages the currently active debugger that supports the Lazarus
  // debug extensions.
  TLazDebugExtensionDebugManager = class
  protected
    class var FActiveDebugger: ILazDebugExtensionDebuggerIntf;
    class var FEventListenerList: TLazDebugEventListenerList;
  public
    class constructor Create;
    class destructor Destroy;
    class procedure MarkActiveDebugger(const ADebugger: ILazDebugExtensionDebuggerIntf);
    class procedure MarkDebuggerDisposed(const ADebugger: ILazDebugExtensionDebuggerIntf);
    class function ActiveDebugger: ILazDebugExtensionDebuggerIntf;

    class procedure AddEventListener(const AListener: TLazDebugEventListenerProc);
    class procedure RemoveEventListener(AListener: TLazDebugEventListenerProc);
    class procedure SendEvent(AnExtDebugEvent: TLazExtDebugEvent);
  end;

implementation

{ TLazDebugExtensionVariable }

constructor TLazDebugExtensionVariable.Create(const AValue, AName, AValueType: string; AReference: PtrInt);
begin
  FValue := AValue;
  FName := AName;
  FValueType := AValueType;
  FReference := AReference;
end;

{ TLazDebugExtensionDebugManager }

class function TLazDebugExtensionDebugManager.ActiveDebugger: ILazDebugExtensionDebuggerIntf;
begin
  Result := FActiveDebugger;
end;

class procedure TLazDebugExtensionDebugManager.MarkDebuggerDisposed(const ADebugger: ILazDebugExtensionDebuggerIntf);
begin
  if ADebugger = FActiveDebugger then
    FActiveDebugger := nil;
end;

class procedure TLazDebugExtensionDebugManager.MarkActiveDebugger(const ADebugger: ILazDebugExtensionDebuggerIntf);
begin
  FActiveDebugger := ADebugger;
end;

class procedure TLazDebugExtensionDebugManager.AddEventListener(const AListener: TLazDebugEventListenerProc);
begin
  if FEventListenerList.IndexOf(AListener) = -1 then
    FEventListenerList.Add(AListener);
end;

class procedure TLazDebugExtensionDebugManager.RemoveEventListener(AListener: TLazDebugEventListenerProc);
begin
  FEventListenerList.Remove(AListener);
end;

class destructor TLazDebugExtensionDebugManager.Destroy;
begin
  FEventListenerList.Free;
end;

class constructor TLazDebugExtensionDebugManager.Create;
begin
  FEventListenerList := TLazDebugEventListenerList.Create;
end;

class procedure TLazDebugExtensionDebugManager.SendEvent(AnExtDebugEvent: TLazExtDebugEvent);
var
  i: Integer;
begin
  for i := 0 to FEventListenerList.Count -1 do
    FEventListenerList.Items[i](AnExtDebugEvent);
end;

end.

