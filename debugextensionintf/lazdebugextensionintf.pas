{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit LazDebugExtensionIntf;

{$warn 5023 off : no warning about unused units}
interface

uses
  LazDebugExtensionDebuggerIntf, LazDebugExtensionEvaluateDlg, LazDebugExtensionVariablesDlg, 
  LazDebugExtensionWatchesDlg, LazDebugExtensionStringTreeController, LazDebugExtensionClasses, 
  LazDebugExtensionStringTreeExpressionController, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('LazDebugExtensionEvaluateDlg', @LazDebugExtensionEvaluateDlg.Register);
  RegisterUnit('LazDebugExtensionVariablesDlg', @LazDebugExtensionVariablesDlg.Register);
  RegisterUnit('LazDebugExtensionWatchesDlg', @LazDebugExtensionWatchesDlg.Register);
end;

initialization
  RegisterPackage('LazDebugExtensionIntf', @Register);
end.
