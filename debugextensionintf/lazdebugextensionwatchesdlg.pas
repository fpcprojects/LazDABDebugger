{ Additional watches-dialog, which shows the data in a tree-like manner.

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}
unit LazDebugExtensionWatchesDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ComCtrls,
  ActnList,
  IDEWindowIntf,
  IDEImagesIntf,
  LazIDEIntf,
  ProjectIntf,
  MenuIntf,
  laz.VirtualTrees,
  LazDebugExtensionClasses,
  LazDebugExtensionStringTreeExpressionController;

type

  { TDebugExtensionWatchesDlg }

  TDebugExtensionWatchesDlg = class(TForm)
    vstWatches: TLazVirtualStringTree;
    ActionList1: TActionList;
    actAddWatch: TAction;
    actDeleteSelected: TAction;
    actDeleteAll: TAction;
    actProperties: TAction;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    ToolButtonTrash: TToolButton;
    ToolButton6: TToolButton;
    ToolButtonTrashAll: TToolButton;
    ToolButton10: TToolButton;
    ToolButtonAdd: TToolButton;
    ToolButtonProperties: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actAddWatchExecute(Sender: TObject);
    procedure actDeleteSelectedExecute(Sender: TObject);
    procedure actDeleteAllExecute(Sender: TObject);
    procedure actPropertiesExecute(Sender: TObject);
  private
  protected
    FWatchesList: TStrings;
    FController: TLazDebugExtensionStringTreeExpressionController;
    procedure WatchListUpdated;
    function OnProjectOpened(Sender: TObject; AProject: TLazProject): TModalResult;
  public
    procedure AddWatch(AWatch: string);
  end;
  TDebugExtensionWatchesDlgSingleton = specialize TDebugExtensionWindowSingleton<TDebugExtensionWatchesDlg>;

procedure Register;

implementation

{$R *.lfm}

resourcestring
  sWatches = 'Watches';
  sWatchesCaption = 'Tree-based watches';
  sBtnAdd = '&Add';
  sAddWatchCaption = 'Add watch';
  sItemAddHint = 'Add';
  sAddWatch = 'Enter the expression you want to add as a watch';
  sBtnDelete = '&Delete';
  sItemDeleteHint = 'Delete';
  sProperties = '&Change';
  sPropertiesHint = 'Change';
  sBtnDeleteAll = 'De&lete';
  sDeleteHint = 'Delete';

const
  TREE_Watches = 'TREE_Watches';

var
  WatchesDlgWindowCreator: TIDEWindowCreator;

procedure CreateDebugDialog(Sender: TObject; aFormName: string; var AForm: TCustomForm; DoDisableAutoSizing: boolean);
begin
  AForm := TDebugExtensionWatchesDlgSingleton.Instance;
  AForm.Name := aFormName;
  AForm.DisableAutoSizing;
  if not DoDisableAutoSizing then
    AForm.EnableAutoSizing;
  IDEWindowCreators.ShowForm(AForm, True, vmOnlyMoveOffScreenToVisible);
end;

procedure ShowWatchesDialog(Sender: TObject);
var
  Frm: TCustomForm;
begin
  Frm := nil;
  CreateDebugDialog(Sender, 'ExtWatches', Frm, False);
end;

procedure Register;
begin
  RegisterIdeMenuCommand(itmViewDebugWindows, sWatches, sWatchesCaption, nil, @ShowWatchesDialog);

  WatchesDlgWindowCreator := IDEWindowCreators.Add('ExtWatches');
  WatchesDlgWindowCreator.OnCreateFormProc := @CreateDebugDialog;
  WatchesDlgWindowCreator.CreateSimpleLayout;

  LazarusIDE.AddHandlerOnProjectOpened(@TDebugExtensionWatchesDlgSingleton.Instance.OnProjectOpened);
end;

{ TDebugExtensionWatchesDlg }

procedure TDebugExtensionWatchesDlg.FormCreate(Sender: TObject);
begin
  FController := TLazDebugExtensionStringTreeExpressionController.Create(vstWatches);

  FWatchesList := TStringList.Create;

  ActionList1.Images := IDEImages.Images_16;
  ToolBar1.Images := IDEImages.Images_16;

  actAddWatch.Caption:=sBtnAdd;
  actAddWatch.Hint:=sItemAddHint;
  actAddWatch.ImageIndex := IDEImages.LoadImage('laz_add');

  actDeleteSelected.Caption := sBtnDelete;
  actDeleteSelected.Hint := sItemDeleteHint;
  actDeleteSelected.ImageIndex := IDEImages.LoadImage('laz_delete');

  actProperties.Caption := sProperties;
  actProperties.Hint := sPropertiesHint;
  actProperties.ImageIndex := IDEImages.LoadImage('menu_environment_options');

  actDeleteAll.Caption := sBtnDeleteAll;
  actDeleteAll.Hint := sDeleteHint;
  actDeleteAll.ImageIndex := IDEImages.LoadImage('menu_clean');
end;

procedure TDebugExtensionWatchesDlg.FormDestroy(Sender: TObject);
begin
  FController.Destroy;
  FWatchesList.Free;
end;

procedure TDebugExtensionWatchesDlg.actAddWatchExecute(Sender: TObject);
var
  Value: string;
begin
  if InputQuery(sAddWatchCaption, sAddWatch, Value) then
    begin
    AddWatch(Value);
    end;
end;

procedure TDebugExtensionWatchesDlg.AddWatch(AWatch: string);
begin
  if AWatch<>'' then
    begin
    FWatchesList.Add(AWatch);
    WatchListUpdated;
    end;
end;

procedure TDebugExtensionWatchesDlg.actDeleteSelectedExecute(Sender: TObject);
var
  SelectedNode: PVirtualNode;
  ExprNode: PVirtualNode;
  DeleteNode: PVirtualNode;
  DeleteExpr: string;
  DeleteInd: Integer;
begin
  SelectedNode := vstWatches.GetFirstSelected;
  while Assigned(SelectedNode) do
    begin
    ExprNode := SelectedNode;
    DeleteExpr := FController.GetExpressionBoundToNode(ExprNode);
    while (DeleteExpr='') and Assigned(ExprNode) do
      begin
      ExprNode := ExprNode^.Parent;
      if Assigned(ExprNode) then
        DeleteExpr := FController.GetExpressionBoundToNode(ExprNode);
      end;

    if Assigned(ExprNode) then
      begin
      DeleteInd := FWatchesList.IndexOf(DeleteExpr);
      if DeleteInd > 0 then
        FWatchesList.Delete(DeleteInd);
      DeleteNode := ExprNode;
      SelectedNode := vstWatches.GetNextSelected(SelectedNode);
      vstWatches.DeleteNode(DeleteNode);
      end
    else
      SelectedNode := vstWatches.GetNextSelected(SelectedNode);
    end;
  WatchListUpdated
end;

procedure TDebugExtensionWatchesDlg.actDeleteAllExecute(Sender: TObject);
begin
  FWatchesList.Clear;
  WatchListUpdated;
end;

procedure TDebugExtensionWatchesDlg.actPropertiesExecute(Sender: TObject);
var
  SelectedNode: PVirtualNode;
  EditExpr: string;
  EditInd: Integer;
begin
  if vstWatches.SelectedCount=1 then
    begin
    SelectedNode := vstWatches.GetFirstSelected;

    EditExpr := FController.GetExpressionBoundToNode(SelectedNode);
    while (EditExpr='') and Assigned(SelectedNode) do
      begin
      SelectedNode := SelectedNode^.Parent;
      if Assigned(SelectedNode) then
        EditExpr := FController.GetExpressionBoundToNode(SelectedNode);
      end;

    if Assigned(SelectedNode) then
      begin
      EditInd := FWatchesList.IndexOf(EditExpr);
      if InputQuery(sAddWatchCaption, sAddWatch, EditExpr) then
        FWatchesList[EditInd] := EditExpr;
      end;
    WatchListUpdated;
    end;
end;

function TDebugExtensionWatchesDlg.OnProjectOpened(Sender: TObject; AProject: TLazProject): TModalResult;
begin
  FWatchesList.DelimitedText := AProject.CustomSessionData.Values[TREE_Watches];
  FController.AssignExpressionList(FWatchesList);
  Result := mrOK;
end;

procedure TDebugExtensionWatchesDlg.WatchListUpdated;
begin
  FController.AssignExpressionList(FWatchesList);
  LazarusIDE.ActiveProject.CustomSessionData.Values[TREE_Watches] := FWatchesList.DelimitedText;
end;

end.

