{ The Lazarus Ide requires some in-between classes to bind debug information
  to the GUI.

  Copyright (C) 2021 Joost van der Sluis (CNOC) joost@cnoc.nl

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}

unit LazDabGlue;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Maps,
  DABMessages,
  LazDebuggerIntf,
  LazDebuggerIntfBaseTypes,
  DbgIntfDebuggerBase;

type
  TLazDABThreadsCallback = procedure(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AThreadList:TDABThreadList) of object;

  { TLazDabBreakpoint }

  TLazDabBreakpoint = class(TDBGBreakPoint)
  private
    FDABId: integer;
  protected
    procedure DoEnableChange; override;
  public
    destructor Destroy; override;
    procedure BPSetValid(const AValue: TValidState);
    property DABId: integer read FDABId write FDABId;
  end;

  { TLazDABCallStackSupplier }

  TLazDABCallStackSupplier = class(TCallStackSupplier)
  private
    FRequestCurrent: Boolean;
    FCurrentStackFrames: TDABStackFrameList;
  protected
    procedure RequestCallstack(AThreadId, ACount: Integer);
  public
    procedure RequestCount(ACallstack: TCallStackBase); override;
    procedure RequestAtLeastCount(ACallstack: TCallStackBase; ARequiredMinCount: Integer); override;
    procedure RequestEntries(ACallstack: TCallStackBase); override;

    procedure HandleCallstackResponse(AThreadId: Integer; ACallstack: TCallStackBase; AStackFrames: TDABStackTraceResponseBody);
    procedure RequestCurrentLocation(AThreadId: Integer);
  end;

  { TLazDABThreadsSupplier }

  TLazDABThreadsSupplier = class(TThreadsSupplier)
  protected
    FLatestRequest: PtrInt;
    procedure OnThreadsResponse(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AThreadList:TDABThreadList);
  public
    procedure SetCurrentThread(AThreadId: Integer);
    procedure RequestMasterData; override;
  end;

implementation

uses
  LazDABCustomDebugger;

{ TLazDABThreadsSupplier }

procedure TLazDABThreadsSupplier.RequestMasterData;
begin
  FLatestRequest := (Debugger as TLazCustomDABDebugger).RequestThreads(@OnThreadsResponse);
end;

procedure TLazDABThreadsSupplier.OnThreadsResponse(const ARef: PtrInt; const ASucceeded: Boolean; const Message: string; const AThreadList: TDABThreadList);
var
  i: Integer;
  Thread: TDABThread;
begin
  if CurrentThreads = nil then Exit;

  if ARef<>FLatestRequest then
    Exit;

  if not ASucceeded then begin
    CurrentThreads.SetValidity(ddsInvalid);
    Exit;
  end;


  CurrentThreads.Clear;
  for i := 0 to AThreadList.Count - 1 do
    begin
    Thread := AThreadList[i];
    CurrentThreads.Add(TThreadEntry.Create(0, nil, '', '', '', -1, Thread.Id, Thread.Name, ''));
    end;

  CurrentThreads.SetValidity(ddsValid);
end;

procedure TLazDABThreadsSupplier.SetCurrentThread(AThreadId: Integer);
begin
  if CurrentThreads = nil then Exit;

  CurrentThreads.CurrentThreadId := AThreadId;
end;

{ TLazDABCallStackSupplier }

procedure TLazDABCallStackSupplier.HandleCallstackResponse(AThreadId: Integer; ACallstack: TCallStackBase; AStackFrames: TDABStackTraceResponseBody);
begin
  if AStackFrames.StackFrames.Count=0 then
    begin
    ACallstack.SetCountValidity(ddsInvalid);
    ACallstack.SetHasAtLeastCountInfo(ddsInvalid);
    Exit;
    end;

  FCurrentStackFrames := AStackFrames.StackFrames;
  try
    if (AStackFrames.TotalFrames > 0) then
      begin
      if (AStackFrames.StackFrames.Count < AStackFrames.TotalFrames) then
        ACallstack.SetHasAtLeastCountInfo(ddsValid, AStackFrames.StackFrames.Count)
      else
        begin
        ACallstack.Count := AStackFrames.StackFrames.Count;
        ACallstack.SetCountValidity(ddsValid);
        end;
      end
    else
      begin
      ACallstack.SetHasAtLeastCountInfo(ddsValid, AStackFrames.StackFrames.Count)
      end;
    ACallstack.DoEntriesUpdated;
  finally
    FCurrentStackFrames := nil;
  end;
end;

procedure TLazDABCallStackSupplier.RequestCount(ACallstack: TCallStackBase);
begin
  if (Debugger = nil) or not(Debugger.State in [dsPause, dsInternalPause]) then
  begin
    ACallstack.SetCountValidity(ddsInvalid);
    Exit;
  end;

  RequestCallstack(ACallstack.ThreadId, 10);
end;

procedure TLazDABCallStackSupplier.RequestCurrentLocation(AThreadId: Integer);
begin
  FRequestCurrent := True;
  // Trigger a callstack-request
  RequestAtLeastCount(CurrentCallStackList.EntriesForThreads[AThreadId], 20);
end;

procedure TLazDABCallStackSupplier.RequestAtLeastCount(ACallstack: TCallStackBase; ARequiredMinCount: Integer);
begin
  if (Debugger = nil) or not(Debugger.State in [dsPause, dsInternalPause]) then
  begin
    ACallstack.SetHasAtLeastCountInfo(ddsInvalid);
    Exit;
  end;

  RequestCallstack(ACallstack.ThreadId, ARequiredMinCount);
end;

procedure TLazDABCallStackSupplier.RequestCallstack(AThreadId, ACount: Integer);
begin
  (Debugger as TLazCustomDABDebugger).RequestCallstack(
    AThreadId,
    ACount,
    CurrentCallStackList.EntriesForThreads[AThreadId],
    FRequestCurrent);
  FRequestCurrent := False;
end;

procedure TLazDABCallStackSupplier.RequestEntries(ACallstack: TCallStackBase);
var
  e: TCallStackEntry;
  i: integer;
  StackFrame: TDABStackFrame;
begin
  if Assigned(FCurrentStackFrames) then
    begin
    for i := 0 to FCurrentStackFrames.Count-1 do
      begin
      StackFrame := FCurrentStackFrames[i];
      e := ACallstack.Entries[i];
      e.Init(0, nil, StackFrame.Name, StackFrame.Source.Name, StackFrame.Source.Path, StackFrame.Line, ddsValid);
      end;
    end;
end;

{ TLazDabBreakpoint }

procedure TLazDabBreakpoint.BPSetValid(const AValue: TValidState);
begin
  SetValid(AValue);
end;

destructor TLazDabBreakpoint.Destroy;
begin
  // Do not remove the breakpoint. Just wait until the breakpoint is hit and then
  // ignore it.
  inherited Destroy;
end;

procedure TLazDabBreakpoint.DoEnableChange;
begin
  inherited DoEnableChange;
  if not (Debugger.State in [dsNone, dsIdle, dsInit]) then
    (Debugger as TLazCustomDABDebugger).SetBreakpoints;
end;

end.

